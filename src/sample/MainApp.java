package sample;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import sample.model.MySQLQuery;
import sample.view.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;




public class MainApp extends Application {

    private static String userID;
    private Stage primaryStage;
    private BorderPane rootLayout;

    public static String adjustAddress(String address, String town) {


        //String urlString = "http://www.mapquestapi.com/geocoding/v1/address?key=NgNSTQM96eAoTfKuam9DEB2AmeRxGymp&location=" + town + "," + address.replaceAll("\\s+", "") + "&outFormat=xml";
        String urlString = "http://www.mapquestapi.com/geocoding/v1/address?key=NgNSTQM96eAoTfKuam9DEB2AmeRxGymp&location="+ town +"," + address.replaceAll("\\s","") + "&outFormat=xml";
        String adjustedAddress = address;

        URL url = null;
        try {
            url = new URL(urlString);
            URLConnection conn = url.openConnection();
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(conn.getInputStream());
            doc.getDocumentElement().normalize();
            NodeList parsonList = doc.getElementsByTagName("response");


            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);
            adjustedAddress = writer.toString().substring(writer.toString().indexOf("<street>") + 8, writer.toString().indexOf("</street>"));
            System.out.println("XML IN String format is: \n" + adjustedAddress);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return adjustedAddress;
    }


    //Il metodo in questione riporta all'interno dello stage un BorderPane, questo sarà la base su cui andare ad incastrare le schermate
    //Pagina di Login, di Registrazione, ecc...

    public static void main(String[] args) {


        String urlString = "http://www.mapquestapi.com/geocoding/v1/address?key=NgNSTQM96eAoTfKuam9DEB2AmeRxGymp&location=Caianello,Ceraselle&outFormat=xml";

        URL url = null;
        try {
            url = new URL(urlString);
            URLConnection conn = url.openConnection();
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(conn.getInputStream());
            doc.getDocumentElement().normalize();
            NodeList parsonList = doc.getElementsByTagName("response");
            String lat = "ciau";
            String longi = "ciau";

            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);
            System.out.println("XML IN String format is: \n" + (writer.toString().substring(writer.toString().indexOf("<street>") + 8, writer.toString().indexOf("</street>"))));
            //writer.toString().indexOf("<street>");//dove inizia questa. Qui devo considerare il valore che mi da + 5 = caratteri di <lat>
            //writer.toString().indexOf("</lat>");//dove inizia questa
            //System.out.println(org.apache.lucene.util.SloppyMath.haversinMeters(41.30454, 14.08184, 41.30705, 14.10519)/1000);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }


        //String urlString = "http://www.mapquestapi.com/geocoding/v1/address?key=NgNSTQM96eAoTfKuam9DEB2AmeRxGymp&location=" + town + "," + address.replaceAll("\\s+", "") + "&outFormat=xml";
        urlString = "http://www.mapquestapi.com/geocoding/v1/address?key=NgNSTQM96eAoTfKuam9DEB2AmeRxGymp&location=Caianello,Ceraselle&outFormat=xml";


        url = null;
        try {
            url = new URL(urlString);
            URLConnection conn = url.openConnection();
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(conn.getInputStream());
            doc.getDocumentElement().normalize();
            NodeList parsonList = doc.getElementsByTagName("response");


            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);
            System.out.println("XML IN String format is: \n" + (writer.toString().substring(writer.toString().indexOf("<street>") + 8, writer.toString().indexOf("</street>"))));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }


        /*String url2 = "https://www.mapquestapi.com/staticmap/v5/map?key=NgNSTQM96eAoTfKuam9DEB2AmeRxGymp&start=Capodichino,Napoli|marker-A20000-D51A1A&end=Caianello,CE|marker-008211-07e625&declutter=true&MA&traffic=flow|con|inc&format=gif&size=800,600@2x";

        try {
            downloadFile(new URL(url2),"prova.gif");
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        String url2 = "https://www.mapquestapi.com/staticmap/v5/map?key=NgNSTQM96eAoTfKuam9DEB2AmeRxGymp&start=Cassino,FR|marker-A20000-D51A1A&end=Via Ceraselle, Caianello|marker-008211-07e625&declutter=true&MA&traffic=flow|con|inc&format=gif&size=800,600@2x";
        System.out.println(StringUtils.deleteWhitespace(url2));



            /*URL obj = null;
            obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));

            String inputLine;
            StringBuffer response = new StringBuffer();
            while((inputLine = in.readLine()) != null){
                response.append(inputLine);
            }

            in.close();*/


        launch(args);


    }

    public static void downloadFile(URL url, String fileName) throws IOException {
        FileUtils.copyURLToFile(url, new File(fileName));
    }

    //Il metodo in questione carica e alloca in memoria la scena contenente la pagina di login
    //successivamente posiziona la scena realizzata nella parte centrale del BorderPain precedentemente
    //caricato all'interno della finestra principale dell'applicazione

    @Override
    public void start(Stage primaryStage) throws Exception {


        //Ogni applicazione presenta una finestra principale (questa la si può vedere come uno stage, ossia un palco)
        //tutti gli oggetti della scena, ovvero tutti gli elementi grafici introdotti nella 'Scena' da mostrare nella finestra principale
        //risulteranno essere allocati in una regione accessibile per mezzo dell'handler primaryStage. Con la seguente istruzione si sta facendo in modo
        //che l'area di memoria menzionata sia referenziata dal suddetto handler
        this.primaryStage = primaryStage;

        //Con questa istruzione si attribuisce un 'Titolo' alla finestra principale dell'applicazione, questo titolo figurerà in alto a destra, nella barra
        //contenente i comandi per 'ridurre a icona' / 'espandere' / 'chiudere' l'applicazione
        this.primaryStage.setTitle("MeetFly");

        //I metodi che seguono allocano in memoria gli oggetti che andranno a costituire la schermata di login dell'applicazione (ovvero quella che appare all'apertura)
        //li allocano ed effettuano la costruzione della 'Scena', scena che poi provvedono a collocare nel 'PrimaryStage'
        initBaseWindow();
        initLoginWindow();

        //this.primaryStage.setResizable(false);
        //this.primaryStage.initStyle(StageStyle.UNDECORATED);

        //Il metodo 'show' rende visibile lo stage realizzato
        this.primaryStage.show();
    }


    //Il metodo in questione carica e alloca in memoria la scena contenente la pagina di registrazione
    //successivamente posiziona la scena realizzata nella parte centrale del BorderPain precedentemente
    //caricato all'interno della finestra principale dell'applicazione

    public void initBaseWindow() {

        try {

            //Con la seguente istruzione si sta istanziando un oggetto della classe FXMLLoader, ovvero un parser / interprete di file fxml
            //tale interprete sarà referenziato dall'handler denominato baseWindowLoader

            FXMLLoader baseWindowLoader = new FXMLLoader();

            //Con la prima istruzione si sta indicando al parser il luogo in cui è posto il file fxml d'analizzare
            //Con la seconda istruzione invece, si sta incaricando il parser di recuperare le informazioni sulla scena trascritte nel file
            //fxml e di allocarle in memoria. L'area contenente queste informazioni sarà accessibile per mezzo dell'handler 'rootLayout' che figura come
            //elemento della classe MainApp. Il tipo di quest'ultimo coincide con quello del nodo definito nel file fxml

            baseWindowLoader.setLocation(MainApp.class.getResource("view/baseWindow.fxml"));
            rootLayout = baseWindowLoader.load();

            //Con la seguente istruzione si sta richiamando il costruttore della classe 'Scene' e lo si sta incaricando di costruire una scena sulla base
            //delle informazioni contenute nell'area referenziata dal rootLayout

            Scene scene = new Scene(rootLayout);

            //Con questa istruzione si sta riportando la scena precedentemente realizzata all'interno della finestra principale dell'applicazione
            primaryStage.setScene(scene);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    //Il metodo in questione carica e alloca in memoria la scena contenente la pagina per il recupero della password
    //successivamente posiziona la scena realizzata nella parte centrale del BorderPain precedentemente
    //caricato all'interno della finestra principale dell'applicazione

    public void initLoginWindow() {

        try {
            FXMLLoader loginLoader = new FXMLLoader();
            loginLoader.setLocation(getClass().getResource("view/loginWindow.fxml"));

            //Dato che il root node della scena descritta all'interno del suddetto file fxml è un AnchorPane,
            //si definisce un handler di questo tipo e gli si associa l'area di memoria contenente le informazioni
            //recuperate dal parser. Il downcasting ha esito positivo proprio per via del fatto che il root node è del tipo AnchorPane

            AnchorPane loginAnchorPane = (AnchorPane) loginLoader.load();
            System.out.println("prova");
            rootLayout.setCenter(loginAnchorPane);
            loginController controller = loginLoader.getController();
            controller.setMainApp(this);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void initFailedRegistrationRequestWindow() {

        boolean loaded = false;

        try {
            FXMLLoader failedRegistrationLoader = new FXMLLoader();
            failedRegistrationLoader.setLocation(getClass().getResource("view/failedRegistrationRequestWindow.fxml"));

            //Dato che il root node della scena descritta all'interno del suddetto file fxml è un AnchorPane,
            //si definisce un handler di questo tipo e gli si associa l'area di memoria contenente le informazioni
            //recuperate dal parser. Il downcasting ha esito positivo proprio per via del fatto che il root node è del tipo AnchorPane

            AnchorPane failedRegistrationAnchorPane = (AnchorPane) failedRegistrationLoader.load();
            System.out.println("FAIL");
            rootLayout.setCenter(failedRegistrationAnchorPane);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Il metodo in questione carica e alloca in memoria la scena contenente la pagina per aggiungere nel database richieste di cerco / offro / condivido passaggio
    //successivamente posiziona la scena realizzata nella parte centrale del BorderPain precedentemente
    //caricato all'interno della finestra principale dell'applicazione

    public void initRegister() {

        try {
            FXMLLoader registerLoader = new FXMLLoader();
            registerLoader.setLocation(getClass().getResource("view/registerWindow.fxml"));
            AnchorPane registerAnchorPane = (AnchorPane) registerLoader.load();

            rootLayout.setCenter(registerAnchorPane);
            registerController controller = registerLoader.getController();
            controller.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //Il metodo in questione carica e alloca in memoria la scena contenente la pagina principale dell'applicazione,
    //(questa è costituita da un menù e da una finestra di benvenuto).
    //Successivamente posiziona la finestra di benvenuto nella parte centrale del BorderPain e il menu nella parte sinistra

    public void initForgottenPwd() {

        try {
            FXMLLoader forgottenPwdLoader = new FXMLLoader();
            forgottenPwdLoader.setLocation(getClass().getResource("view/forgottenPwdWindow.fxml"));
            AnchorPane forgottenPwdAnchorPane = (AnchorPane) forgottenPwdLoader.load();

            rootLayout.setCenter(forgottenPwdAnchorPane);
            forgottenPwdController controller = forgottenPwdLoader.getController();
            controller.setMainApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Il metodo in questione carica e alloca in memoria la scena contenente la pagina profilo, ovvero la pagina contenente i dati principali di ciascun utente
    //successivamente posiziona la scena realizzata nella parte centrale del BorderPain precedentemente
    //caricato all'interno della finestra principale dell'applicazione

    public void initAddRequest() {

        try {
            FXMLLoader addRequestLoader = new FXMLLoader();
            addRequestLoader.setLocation(getClass().getResource("view/addRequestWindow.fxml"));
            AnchorPane addRequestAnchorPane = (AnchorPane) addRequestLoader.load();

            rootLayout.setCenter(addRequestAnchorPane);

            addRequestWindowController controller = addRequestLoader.getController();
            controller.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initMainWindow() {

        try {
            FXMLLoader menuLoader = new FXMLLoader();
            menuLoader.setLocation((getClass().getResource("view/menuWindow.fxml")));
            AnchorPane menuAnchorPane = (AnchorPane) menuLoader.load();

            FXMLLoader welcomeLoader = new FXMLLoader();
            welcomeLoader.setLocation((getClass().getResource("view/welcomeWindow.fxml")));
            AnchorPane welcomeAnchorPane = (AnchorPane) welcomeLoader.load();

            rootLayout.setLeft(menuAnchorPane);
            rootLayout.setCenter(welcomeAnchorPane);

            menuWindowController controller = menuLoader.getController();
            controller.setMainApp(this);

            controller.setLoggedUser();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void initProfileWindow() {

        try {

            FXMLLoader profileLoader = new FXMLLoader();
            profileLoader.setLocation(getClass().getResource("view/profileWindow.fxml"));
            AnchorPane profileAnchorPane = (AnchorPane) profileLoader.load();
            //System.out.println("Check");
            rootLayout.setCenter(profileAnchorPane);
            //System.out.println("Check");
            profileWindowController controller = profileLoader.getController();
            controller.setMainApp(this);
            controller.initField();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initMessagesWindow() {

        try {
            FXMLLoader messagesWindowLoader = new FXMLLoader();
            messagesWindowLoader.setLocation(getClass().getResource("view/receivedMessagesWindow.fxml"));
            AnchorPane messagesWindowAnchorPane = (AnchorPane) messagesWindowLoader.load();

            rootLayout.setCenter(messagesWindowAnchorPane);

            receivedMessagesWindowController controller = messagesWindowLoader.getController();
            controller.setMainApp(this);
            controller.loadMessages();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initConversationWindow(String user) {


        try {

            FXMLLoader conversationWindowLoader = new FXMLLoader();
            conversationWindowLoader.setLocation(getClass().getResource("view/conversationWindow.fxml"));
            AnchorPane conversationWindowAnchorPane = (AnchorPane) conversationWindowLoader.load();

            int num = conversationWindowAnchorPane.getChildren().size();

            for (int i = 0; i < num; i++) {

                if (conversationWindowAnchorPane.getChildren().get(i) != null && conversationWindowAnchorPane.getChildren().get(i).getId().matches("titleField")) {
                    Label title = (Label) conversationWindowAnchorPane.getChildren().get(i);
                    title.setText("Conversation with " + user + ": ");
                    break;
                }
            }

            rootLayout.setCenter(conversationWindowAnchorPane);

            conversationWindowController controller = conversationWindowLoader.getController();
            controller.setMainApp(this);
            controller.setUser(user);

            MioThread thread = new MioThread(controller, user, getUserID());
            thread.setDaemon(true);
            thread.start();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void exit() {
        rootLayout.setLeft(null);
        initLoginWindow();
        setUserID("guest");
    }

    //Il metodo in questione consente di ottenere in qualsiasi punto dell'applicazione l'ID dell'utente connesso
    public String getUserID() {
        return userID;
    }

    //Il metodo in questione viene eseguito nell'istante in cui si riesce ad effettuare il login con successo,
    //esso associa l'handler 'userID' definito in questa classe ad un oggetto String inizializzato con il testo
    //introdotto nel campo nickname presente nella finestra di login.
    public void setUserID(String ID) {
        userID = ID;
    }

    public void prova() {
        try {
            FXMLLoader addRequestLoader = new FXMLLoader();

            addRequestLoader.setLocation(getClass().getResource("view/prova.fxml"));
            AnchorPane addRequestAnchorPane = (AnchorPane) addRequestLoader.load();

            rootLayout.setCenter(addRequestAnchorPane);

            myRequestsWindow controller = addRequestLoader.getController();
            controller.setMainApp(this);

            controller.showAllMyRequests();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initSearchRequestWindow() {
        try {
            FXMLLoader searchRequestLoader = new FXMLLoader();

            searchRequestLoader.setLocation(getClass().getResource("view/searchRequestWindow.fxml"));
            AnchorPane searchRequestAnchorPane = (AnchorPane) searchRequestLoader.load();

            rootLayout.setCenter(searchRequestAnchorPane);

            searchWindowController controller = searchRequestLoader.getController();
            controller.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initResultsSearchWindow(ResultSet rs, String myDestination, String myTown) {
        try {

            FXMLLoader resultSearchRequestLoader = new FXMLLoader();
            resultSearchRequestLoader.setLocation(getClass().getResource("view/prova.fxml"));
            AnchorPane resultSearchRequestAnchorPane = (AnchorPane) resultSearchRequestLoader.load();

            rootLayout.setCenter(resultSearchRequestAnchorPane);

            myRequestsWindow controller = resultSearchRequestLoader.getController();
            controller.setMainApp(this);
            controller.setMyDestination(myDestination);
            controller.setMyTown(myTown);

            if (rs != null)
                controller.showAllRequests(rs);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initOutcomeAddRequestWindow(String outcome) {


        try {
            FXMLLoader outcomeLoader = new FXMLLoader();

            if (outcome.matches("done"))
                outcomeLoader.setLocation(getClass().getResource("view/doneRequestWindow.fxml"));
            else
                outcomeLoader.setLocation(getClass().getResource("view/failedRequestWindow.fxml"));

            AnchorPane outcomeAnchorPane = (AnchorPane) outcomeLoader.load();
            rootLayout.setCenter(outcomeAnchorPane);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initContactWindow(String name) {
        try {

            Stage contactWindow = new Stage();
            FXMLLoader contactWindowLoader = new FXMLLoader();
            contactWindowLoader.setLocation(getClass().getResource("view/contactWindow.fxml"));
            AnchorPane contactWindowAnchorPane = (AnchorPane) contactWindowLoader.load();

            Scene contactWindowScene = new Scene(contactWindowAnchorPane);
            contactWindow.setScene(contactWindowScene);
            contactWindow.initModality(Modality.WINDOW_MODAL);
            contactWindow.initOwner(primaryStage);

            contactWindow.show();

            contactWindowController controller = contactWindowLoader.getController();
            controller.setMainApp(this);
            controller.setRecipient(name);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initMapWindow(String startAddress, String startTown, String endAddress, String endTown){


        try {
            Stage mapWindow = new Stage();
            FXMLLoader mapWindowLoader = new FXMLLoader();
            mapWindowLoader.setLocation(getClass().getResource("view/dynamicMapView.fxml"));
            AnchorPane mapWindowAnchorPane = (AnchorPane) mapWindowLoader.load();

            Scene mapWindowScene = new Scene(mapWindowAnchorPane);
            mapWindow.setScene(mapWindowScene);
            mapWindow.initModality(Modality.WINDOW_MODAL);
            mapWindow.initOwner(primaryStage);
            mapWindow.setResizable(false);

            mapWindow.show();

            dynamicMapViewController controller = mapWindowLoader.getController();
            controller.setMainApp(this);
            controller.loadMap(startAddress,startTown,endAddress,endTown);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    class MioThread extends Thread {

        conversationWindowController controller;
        String author;
        String myID;

        @Override
        public void run() {

            getAllMessages(author, myID);

        }

        public MioThread(conversationWindowController controller, String author, String myID) {
            this.controller = controller;
            this.author = author;
            this.myID = myID;
        }

        public void getAllMessages(String author, String myID) {

            while (true) {

                MySQLQuery query = MySQLQuery.getInstance();

                ResultSet rs1 = query.select("messages", new ArrayList<String>(Arrays.asList("author", "recipient")), new ArrayList<String>(Arrays.asList(author, myID)));
                ResultSet rs2 = query.select("messages", new ArrayList<String>(Arrays.asList("author", "recipient")), new ArrayList<String>(Arrays.asList(myID, author)));
                ArrayList<String> authors = new ArrayList<String>();
                ArrayList<String> messages = new ArrayList<String>();


                try {


                    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                    Boolean rs1NoData = false;
                    Boolean rs2NoData = false;
                    Boolean rs1Entered = false;
                    int num1 = 0; //Numero di Messaggi che ho ricevuto dall'utente il cui nick è stato passato al metodo
                    int num2 = 0; //Numero di Messaggi che ho inviato all'utente il cui nick è stato passato al metodo


                    try {
                        //Verifico se l'utente il cui nick è stato passato al metodo mi ha mai mandato qualche messaggio
                        //se non mi ha mai scritto rs1NoData viene settato a TRUE
                        if (!rs1.next())
                            rs1NoData = true;

                        else
                            num1++;

                        //Verifico se io ho mai scritto all'utente il cui nick è stato passato al metodo
                        //se non ho mai scritto all'utente rs2NoData viene settato a TRUE
                        if (!rs2.next())
                            rs2NoData = true;

                        else
                            num2++;

                        //Conto quanti messaggi mi ha inviato l'utente il cui nick è stato passato al metodo
                        if (!rs1NoData) {
                            while (rs1.next())
                                num1++;

                            //faccio in modo che rs1 faccia riferimento nuovamente alla prima tupla
                            rs1.first();

                        }

                        //Conto quanti messaggi ho inviato all'utente il cui nick è stato passato al metodo
                        if (!rs2NoData) {

                            while (rs2.next())
                                num2++;

                            //faccio in modo che rs2 faccia riferimento nuovamente alla prima tupla
                            rs2.first();
                        }

                        while (num1 > 0 || num2 > 0) {


                            //Si eseguono le istruzioni all'interno del blocco associato al seguente costrutto if se
                            //c'è stato uno scambio di messaggi tra me e l'utente il cui nick è stato passato al metodo
                            //in questione

                            if (!rs1NoData && !rs2NoData) {

                                LocalDateTime other = LocalDateTime.parse(rs1.getString("dateAndTime"), format);
                                LocalDateTime mine = LocalDateTime.parse(rs2.getString("dateAndTime"), format);


                                if ((other.isBefore(mine) && num1 > 0) || (!rs1Entered && num2 == 0 )) {

                                    authors.add(author);
                                    messages.add(rs1.getString("text"));
                                    num1--;


                                    if (num1 > 0)
                                        rs1.next();

                                    else if(num1 == 0)
                                        rs1Entered = true;

                                } else if (num2 > 0) {

                                    authors.add(myID);
                                    messages.add(rs2.getString("text"));
                                    num2--;

                                    if (num2 > 0)
                                        rs2.next();

                                }
                            } else if (rs1NoData && !rs2NoData) {

                                authors.add(myID);
                                messages.add(rs2.getString("text"));
                                num2--;

                                if (num2 > 0)
                                    rs2.next();

                            } else if (!rs1NoData && rs2NoData) {
                                authors.add(author);
                                messages.add(rs1.getString("text"));
                                num1--;

                                if (num1 > 0)
                                    rs1.next();
                            }

                        }

                        //AGGIORNAMENTO DELLA CHAT

                        Platform.runLater(() -> {
                            controller.removeAllMessages();
                            System.out.println("CLEAR");
                            controller.addMessage(authors, messages);
                        });


                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    try {
                        currentThread().sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
