package sample.model;
import java.sql.*;


public class DAOMySQLSettings {


    //Le seguenti stringhe contengono sia nome utente (USR) e password (PWD) dell'utente autorizzato ad SELECT, UPDATE ed INSERT
    //sia i dati per instaurare una connessione con il databse, quindi: HOST, nome dello schema (SCHEMA) e parametri vari (PARAMETERS)

    private final static String HOST = "localhost";
    private final static String USR = "guest";
    private final static String PWD = "guest";
    private final static String SCHEMA = "prova";
    public final static String PARAMETERS = "?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";




    //Metodi di Get (Restituiscono il contenuto delle Stringhe: host, usr, pwd e schema)

    public String getHost(){return HOST;}
    public String getUsrName(){return USR;}
    public String getPassword(){return PWD;}
    public String getSchema(){return SCHEMA;}



    //Handle utilizzato per referenziare l'area di memoria contenente i dati di accesso al database

    private static DAOMySQLSettings currentDAOMySQLSettings = null;


    //Il metodo istanzia in memoria un oggetto del tipo DAOMySQLSettings e restituisce l'indirizzo dell'area
    //all'interno della quale questo è stato allocato

    public static DAOMySQLSettings getDefaultDAOMySQLSettings(){

        DAOMySQLSettings settings = new DAOMySQLSettings();
        return settings;
    }


    //Metodo per ottenere un oggetto Statement per l'elaborazione di un'istruzione SQL

    public static Statement getStatement() throws SQLException {

        //Se l'handler 'currentDAOMySQLSettings' non referenzia nessun'area di memoria, s'istanzia in memoria
        //un oggetto DAOMySQLSettings e lo si associa a tale handler.

        if(currentDAOMySQLSettings == null)
            currentDAOMySQLSettings = getDefaultDAOMySQLSettings();

        //La seguente istruzione consente d'instaurare una connessione con il database.
        //Host, Usr, Pwd e Nome dello Schema vengono prelevati dagli omonimi attributi statici della classe, quindi: HOST, SCHEMA, USR ecc..

        /*https://www.ibm.com/support/knowledgecenter/it/ssw_ibm_i_71/rzaha/db2drivr.htm*/

        Connection conn = DriverManager.getConnection("jdbc:mysql://" + currentDAOMySQLSettings.HOST + ":3306" + "/" + currentDAOMySQLSettings.SCHEMA + currentDAOMySQLSettings.PARAMETERS , currentDAOMySQLSettings.USR , currentDAOMySQLSettings.PWD);

        //Si utilizza un oggetto Statement per l'elaborazione di un'istruzione SQL statica e l'ottenimento dei risultati prodotti da questa.
        //Gli oggetti Statement sono creati dagli oggetti Connection con il metodo createStatement
        /*https://www.ibm.com/support/knowledgecenter/it/ssw_ibm_i_71/rzaha/statemnt.htm*/

        Statement  statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_UPDATABLE);

        return  statement;
    }

}
