package sample.model;

import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;
import sample.model.DAOMySQLSettings;

import javafx.scene.control.TextField;
import sample.model.dao.DAO;
import sample.model.dao.DAOException;

import javax.mail.MessagingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.*;


//Classe che implementa l'interfaccia DAO

public class MySQLQuery implements DAO<Users> {


    private static MySQLQuery istance = new MySQLQuery();

    private MySQLQuery() {
    }

    public static MySQLQuery getInstance() {
        return istance;
    }

    //Override del metodo per eseguire SELECT sul database
    //sulla base degli attributi dell'oggetto Users passato

    @Override
    public List<Users> select(Users a) throws DAOException {

        ArrayList<Users> usersList = new ArrayList<Users>();

        try {

            //S'istanzia in memoria un oggetto Statement per processare l'istruzione SQL che segue;

            Statement st = DAOMySQLSettings.getStatement();

            //Si definisce un handler del tipo DAOMySQLSettings e si fa referenziare l'area di memoria contenente
            //i dati del database creato

            DAOMySQLSettings settings = DAOMySQLSettings.getDefaultDAOMySQLSettings();

            //La query che segue consente di prelevare il contenuto di tutte le tuple della tabella specificata

            String query = "SELECT * FROM " + settings.getSchema();

            //Si esegue la query con lo statement precedentemente allocato

            st.executeQuery(query);

            //Un oggetto ResultSet fornisce numerosi metodi per prelevare i dati dalle celle delle righe restituite
            //dalla query precedentemente eseguita

            ResultSet rs = st.getResultSet();

            //Si utilizza il ciclo while per scorrere le righe restituite dalla query precedentemente eseguita
            while (rs.next()) {

                //Istanziamo un oggetto del Tipo Users e inizializziamo i suoi attributi con il contenuto
                //delle celle appartenenti alla riga che stiamo analizzando

                Users users = new Users();
                users.setUsr(rs.getString("username"));
                users.setPassword(rs.getString("password"));
                users.setMail(rs.getString("mail"));
                users.setPhone(rs.getString("phone"));
                users.setName(rs.getString("name"));
                users.setSurname(rs.getString("surname"));

                //Aggiungiamo l'oggetto appena inizializzato, alla lista definita fuori dal ciclo
                usersList.add(users);
            }


        } catch (SQLException e) {
            e.printStackTrace();

        }

        return usersList;

    }

    //Override del metodo per eseguire UPDATE sul database
    @Override
    public void update(Users a, ArrayList<String> c, ArrayList<String> v) throws DAOException {

        try {

            //S'istanzia in memoria un oggetto Statement per processare l'istruzione SQL che segue;

            Statement st = DAOMySQLSettings.getStatement();

            //Si definisce un handler del tipo DAOMySQLSettings e si fa referenziare l'area di memoria contenente
            //i dati del database creato

            DAOMySQLSettings settings = DAOMySQLSettings.getDefaultDAOMySQLSettings();

            //Il numero di elementi presenti nell'ArrayList 'c' corrisponde al numero d'iterazioni eseguite dal seguente ciclo for
            for (int i = 0; i < c.size(); i++) {

                //Quella che segue è una query che consente di aggiornare il campo 'c.get(i)' della tupla (riga) avente
                // come PK 'username', con il valore 'v.get(i)'.
                String update = "UPDATE " + settings.getSchema() + " SET `" + c.get(i) + "` = '" + v.get(i) + "' WHERE (`username` = '" + a.getUsr() + "')";

                //Si utilizza lo statement istanziato per eseguire la suddetta query
                st.executeUpdate(update);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void insert(Users a) throws DAOException {

    }

    @Override
    public void delete(Users a) throws DAOException {

    }


    public void insert(Users a, String table) throws DAOException {

        try {

            //S'istanzia in memoria un oggetto Statement per processare l'istruzione SQL che segue;
            Statement st = DAOMySQLSettings.getStatement();

            //Si definisce un handler del tipo DAOMySQLSettings e si fa referenziare l'area di memoria contenente
            //i dati del database creato
            DAOMySQLSettings settings = DAOMySQLSettings.getDefaultDAOMySQLSettings();

            //Quella che segue è una query che consente di effettuare inserimenti all'interno del database.
            //La query attribuisce ai campi specificati il valore contenuto negli omonimi attributi dell'oggetto 'a'

            String query = "INSERT INTO " + settings.getSchema() + "." + table + "(name, surname, mail, username, password, phone, age, nationality, sex) VALUES ('"
                    + a.getName() + "' , '" + a.getSurname() + "' , '" + a.getMail() + "' , '"
                    + a.getUsr() + "' , '" + a.getPassword() + "' , '" + a.getPhone() + "' , '"
                    + a.getAge() + "' , '" + a.getNationality() + "' , '" + a.getSex() + "');";

            //Si utilizza l'oggetto Statement precedentemente istanziato per eseguire la query
            st.executeUpdate(query);

            try {

                //Si utilizza il metodo 'sendEmail' della classe 'MailSystem'
                //Questo è un metodo 'public' e 'static' quindi lo si può evocare senza creare istanze della classe.
                //Quindi, non serve fare:
                //  MailSystem nome_oggetto = new MailSystem ();
                //  nome_oggetto.sendEmail(...);
                //  possiamo fare direttamente MailSystem.sendEmail(...) , ossia nome_classe.nome.metodo(....)

                MailSystem.sendEmail("smtp.libero.it", "25", "francescabianchiing@libero.it", "Francescabianchi1@",
                        a.getMail(), "BENVENUTO IN MEETFLY", "Benvenuto\\a " + a.getUsr() + "\n" +
                                "\n" +
                                "Grazie per la tua registrazione.\n" +
                                "\n" +
                                "\n" +
                                "\n" +
                                "Questa e-mail è stata inviata all'indirizzo di posta elettronica da te indicato al momento della richiesta di registrazione.\n" +
                                "\n" +
                                "ATTENZIONE:\n" +
                                "\n" +
                                "- questo messaggio è inviato automaticamente.\n" +
                                "- nel caso smarrissi username e/o password, richiedile attraverso l'apposita sezione (Login, Forgotten password? )\n" +
                                "- i dati forniti saranno trattati in conformità con le finalità e con le modalità di cui all' informativa privacy ai sensi dell'articolo 13 del Decreto Legislativo 30 Giugno 2003 N.196.\n" +
                                "\n" +
                                "I migliori saluti,\n" +
                                "Servizio Clienti MeetFly\n" +
                                "\n" +
                                "Grazie per aver scelto MeetFly !\n" +
                                "\n" +
                                "Cordiali saluti");
            } catch (MessagingException e) {
                e.printStackTrace();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public ResultSet select(String table) {


        try {

            Statement st = null;
            st = DAOMySQLSettings.getStatement();
            DAOMySQLSettings settings = DAOMySQLSettings.getDefaultDAOMySQLSettings();

            String query = "SELECT * FROM " + settings.getSchema() + "." + table;
            st.executeQuery(query);

            ResultSet rs = st.getResultSet();

            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    public void insert(String table, ArrayList<String> value, String type) throws SQLException {


        //S'istanzia in memoria un oggetto Statement per processare l'istruzione SQL che segue;
        Statement st = DAOMySQLSettings.getStatement();

        //Si definisce un handler del tipo DAOMySQLSettings e si fa referenziare l'area di memoria contenente
        //i dati del database creato
        DAOMySQLSettings settings = DAOMySQLSettings.getDefaultDAOMySQLSettings();

        if (type.matches("requests")) {
            //Quella che segue è una query che consente di effettuare inserimenti all'interno del database.
            //La query attribuisce ai campi specificati il valore contenuto negli omonimi attributi dell'oggetto 'a'
            String query = "INSERT INTO " + settings.getSchema() + "." + table + "(fromCountry, toCountry, fromAirport, toAirport, arriveDate, arriveTime, town, author, adultsNumber, childrenNumber, serviceType, address) VALUES ('"
                    + value.get(0) + "' , '" + value.get(1) + "' , '" + value.get(2) + "' , '"
                    + value.get(3) + "' , '" + value.get(4) + "' , '" + value.get(5) + "' , '"
                    + value.get(6) + "' , '" + value.get(7) + "' , '" + value.get(8) + "' , '"
                    + value.get(9) + "' , '" + value.get(10) + "' , '" + value.get(11) + "');";

            //Si utilizza l'oggetto Statement precedentemente istanziato per eseguire la query
            st.executeUpdate(query);

        }

        if (type.matches("newUser")) {

            //Quella che segue è una query che consente di effettuare inserimenti all'interno del database.
            //La query attribuisce ai campi specificati il valore contenuto negli omonimi attributi dell'oggetto 'a'
            String query = "INSERT INTO " + settings.getSchema() + "." + table + "(username, mail, name, surname, phone, password, age, nationality) VALUES ('"
                    + value.get(0) + "' , '" + value.get(1) + "' , '" + value.get(2) + "' , '"
                    + value.get(3) + "' , '" + value.get(4) + "' , '" + value.get(5) + "');";

            st.executeUpdate(query);

        }

        if(type.matches("replayMessage")){
            //Quella che segue è una query che consente di effettuare inserimenti all'interno del database.
            //La query attribuisce ai campi specificati il valore contenuto negli omonimi attributi dell'oggetto 'a'
            String query = "INSERT INTO " + settings.getSchema() + "." + table + "(author, recipient, dateAndTime, text, type) VALUES ('"
                    + value.get(0) + "' , '" + value.get(1) + "' , '" + value.get(2) + "' , '"
                    + value.get(3) + "' , '" + "replay" + "');";

            st.executeUpdate(query);
        }

        if(type.matches("newMessage")){
            //Quella che segue è una query che consente di effettuare inserimenti all'interno del database.
            //La query attribuisce ai campi specificati il valore contenuto negli omonimi attributi dell'oggetto 'a'
            String query = "INSERT INTO " + settings.getSchema() + "." + table + "(author, recipient, dateAndTime, text, type) VALUES ('"
                    + value.get(0) + "' , '" + value.get(1) + "' , '" + value.get(2) + "' , '"
                    + value.get(3) + "' , '" + "new" + "');";

            st.executeUpdate(query);
        }

    }

    public void update(String table, ArrayList<String> c, ArrayList<String> v) {
        try {

            //S'istanzia in memoria un oggetto Statement per processare l'istruzione SQL che segue;

            Statement st = DAOMySQLSettings.getStatement();

            //Si definisce un handler del tipo DAOMySQLSettings e si fa referenziare l'area di memoria contenente
            //i dati del database creato

            DAOMySQLSettings settings = DAOMySQLSettings.getDefaultDAOMySQLSettings();


            //Il numero di elementi presenti nell'ArrayList 'c' corrisponde al numero d'iterazioni eseguite dal seguente ciclo for
            for (int i = 1; i < c.size(); i++) {

                if (table.matches("users")) {

                    //Quella che segue è una query che consente di aggiornare il campo 'c.get(i)' della tupla (riga) avente
                    // come PK il campo indicato in c.get(0).
                    String update = "UPDATE " + settings.getSchema() + "." + table + " SET `" + c.get(i) + "` = '" + v.get(i) + "' WHERE (`" + c.get(0) + "` = '" + v.get(0) + "')";
                    st.executeUpdate(update);
                }

                //Si utilizza lo statement istanziato per eseguire la suddetta query

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(String table, ArrayList<String> c, ArrayList<String> v) {

        int dim = c.size();
        String text = "WHERE (`" + c.get(0) + "`='" + v.get(0) + "')";

        for (int i = 1; i < dim; i++)
            text = text + " AND (`" + c.get(i) + "`='" + v.get(i) + "')";

        try {
            Statement st = DAOMySQLSettings.getStatement();

            DAOMySQLSettings settings = DAOMySQLSettings.getDefaultDAOMySQLSettings();

            String delete = "DELETE FROM " + settings.getSchema() + "." + table + " " + text;
            System.out.println(delete);
            st.executeUpdate(delete);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet select(String table, ArrayList<String> field, ArrayList<String> value) {

        ResultSet rs = null;

        try {
            Statement st = DAOMySQLSettings.getStatement();
            DAOMySQLSettings settings = DAOMySQLSettings.getDefaultDAOMySQLSettings();
            int dim = field.size();
            String queryEnd = " WHERE " + field.get(0) + "='" + value.get(0) + "'";

            for (int i = 1; i < dim; i++) {

                if (field.get(i).matches("arriveTime"))
                    queryEnd = queryEnd + " AND " + field.get(i) + " LIKE '" + value.get(i).substring(0, value.get(i).indexOf(":")) + "%'";

                else
                    queryEnd = queryEnd + " AND " + field.get(i) + "='" + value.get(i) + "'";
            }

            System.out.println("SELECT * FROM " + settings.getSchema() + "." + table + queryEnd);
            st.executeQuery("SELECT * FROM " + settings.getSchema() + "." + table + queryEnd);

            rs = st.getResultSet();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rs;

    }

}
