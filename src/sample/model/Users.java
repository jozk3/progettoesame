package sample.model;

//Classe utilizzata per gestire gli utenti dell'applicazione

public class Users {


    private String name;
    private String surname;
    private String mail;
    private String pwd;
    private String phone;
    private String usr;
    private String nationality;
    private String age;
    private String sex;


    public void setName(String name){this.name = name;}
    public void setSurname(String surname){this.surname = surname;}
    public void setUsr(String usr){this.usr = usr;}
    public void setPassword(String pwd){this.pwd = pwd;}
    public void setMail(String mail){this.mail = mail;}
    public void setPhone(String phone){this.phone = phone;}
    public void setNationality(String nationality){this.nationality = nationality;}
    public void setAge(String age){this.age = age;}
    public void setSex(String sex){this.sex = sex;}

    public String getName(){return this.name;}
    public String getSurname(){return this.surname;}
    public String getUsr(){return this.usr;}
    public String getPassword(){return this.pwd;}
    public String getMail(){return this.mail;}
    public String getPhone(){return this.phone;}
    public String getNationality(){return nationality;}
    public String getAge(){return age;}
    public String getSex(){return this.sex;}
}
