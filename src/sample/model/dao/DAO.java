package sample.model.dao;

import java.util.ArrayList;
import java.util.List;

//Template di un'interfaccia per l'implementazione del Design Pattern DAO
//Utilizzare il pattern Data Access Object (DAO) significa codificare a parte le funzioni che consentono all'applicazione di accedere
//al contenuto di una qualsiasi sorgente dati (es. Database)

public interface DAO <T>{
    List<T> select(T a) throws DAOException;
    void update(T a , ArrayList<String> c , ArrayList<String> v) throws DAOException;
    void insert(T a) throws DAOException;
    void delete(T a) throws DAOException;
}
