package sample.model.dao;

//Eccezione lanciata dai metodi delle classi che implementano l'interfaccia DAO

public class DAOException extends Exception{

    private String errorMessage;

    public DAOException(String message){
        this.errorMessage = message;
    }

    public void setErrorMessage(String message){
        this.errorMessage = message;
    }

    public boolean isSQLIntegrityConstraintViolation(){

        if(errorMessage.contains("for key 'PRIMARY'"))
            return true;

        else
            return false;
    }
}