package sample.view;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class Map {

    private static Map s = new Map();

    private Map() {
    }

    public static Map getInstance() {
        return s;
    }

    public String adjustAddress(String address, String town, String element) {


        //String urlString = "http://www.mapquestapi.com/geocoding/v1/address?key=NgNSTQM96eAoTfKuam9DEB2AmeRxGymp&location=" + town + "%2C" + StringUtils.deleteWhitespace(address) + "&outFormat=xml";
        String urlString = "http://www.mapquestapi.com/geocoding/v1/address?key=NgNSTQM96eAoTfKuam9DEB2AmeRxGymp&location="+ town +"," + address.replaceAll("\\s","") + "&outFormat=xml";

        String adjustedAddress = address;
        String adjustedTown = town;

        URL url = null;
        try {
            url = new URL(urlString);
            URLConnection conn = url.openConnection();
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(conn.getInputStream());
            doc.getDocumentElement().normalize();
            NodeList parsonList = doc.getElementsByTagName("response");


            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);
            adjustedAddress = writer.toString().substring(writer.toString().indexOf("<street>") + 8, writer.toString().indexOf("</street>"));
            System.out.println(adjustedAddress);
            adjustedTown = writer.toString().substring(writer.toString().indexOf("\"City\">") + 7, writer.toString().indexOf("</adminArea5>"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        System.out.println(adjustedAddress);
        System.out.println(adjustedTown);

        if (element.matches("town"))
            return adjustedTown;
        else
            return adjustedAddress;
    }

    public boolean verifyDistance(String startLocation, String endLocation) {

        String urlString = "http://www.mapquestapi.com/directions/v2/route?key=NgNSTQM96eAoTfKuam9DEB2AmeRxGymp&from=" + startLocation.replaceAll("\\s" , "%2C") + "&to=" + endLocation.replaceAll("\\s" , "%2C") + "&outFormat=xml";
        Double distance = 0.0;

        if(!startLocation.matches(endLocation)) {
            URL url = null;
            try {
                url = new URL(urlString);
                URLConnection conn = url.openConnection();
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document doc = builder.parse(conn.getInputStream());
                doc.getDocumentElement().normalize();
                NodeList parsonList = doc.getElementsByTagName("response");


                DOMSource domSource = new DOMSource(doc);
                StringWriter writer = new StringWriter();
                StreamResult result = new StreamResult(writer);
                TransformerFactory tf = TransformerFactory.newInstance();
                Transformer transformer = tf.newTransformer();
                transformer.transform(domSource, result);
                String text = writer.toString().substring(writer.toString().indexOf("<distance>") + 10, writer.toString().indexOf("</distance>"));
                distance = Double.parseDouble(text);
                System.out.println(distance);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TransformerConfigurationException e) {
                e.printStackTrace();
            } catch (TransformerException e) {
                e.printStackTrace();
            }
        }

        if(distance < 30)
            return true;

        else
            return false;

    }

}
