package sample.view;

import com.jfoenix.controls.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import sample.MainApp;
import sample.model.MySQLQuery;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;


public class addRequestWindowController implements Initializable {


    //  Le seguenti istruzioni consentono d'istanziare una lista di listener per verificare se il contenuto
    //  degli omonimi campi è cambiato

    ObservableList<String> fromAirportBoxList;
    ObservableList<String> CountryBoxList;
    ObservableList<String> toAirportBoxList;


    //*-----------------Elementi che figurano nella scena realizzata con sceneBuilder-------------------*
    @FXML
    JFXButton addRequestButton;
    @FXML
    JFXDatePicker dateField;
    @FXML
    JFXTimePicker timeField;
    @FXML
    JFXTextField streetField;
    @FXML
    JFXTextField adultsField;
    @FXML
    JFXTextField childrenField;
    @FXML
    TextField townField;
    @FXML
    JFXComboBox fromAirportBox;
    @FXML
    JFXComboBox fromCountryBox;
    @FXML
    JFXComboBox toAirportBox;
    @FXML
    JFXComboBox toCountryBox;
    @FXML
    JFXButton adultsPlusButton;
    @FXML
    JFXButton adultsMinusButton;
    @FXML
    JFXButton childrenPlusButton;
    @FXML
    JFXButton childrenMinusButton;
    @FXML
    JFXCheckBox lookForCheckBox;
    @FXML
    JFXCheckBox offerCheckBox;
    @FXML
    JFXCheckBox shareCheckBox;
    //*-------------------------------------------------------------------------------------------------------*

    //  L'attributo in questione consente al controller di richiamare i metodi definiti nella classe "principale", ovvero la classe eseguibile
    private MainApp mainApp;

    //  Il metodo in questione associa il suddetto handler all'unica istanza della classe MainApp. Istanza che viene creata
    //  nell'istante in cui si esegue l'applicazione
    public void setMainApp(MainApp mainApp) {

        this.mainApp = mainApp;
    }

    //  Il metodo in questione viene eseguito in maniera automatica nell'istante in cui viene istanziato un oggetto di questa classe
    //  Ciò accade quando viene eseguito il metodo showAddRequestWindow.

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {

            //  Con questa istruzione si definisce un handler MySQLQuery e si fa in modo che possa referenziare l'unica istanza di questa classe

            MySQLQuery query = MySQLQuery.getInstance();

            //  Con questa istruzione si esegue un SELECT sulla tabella airport

            ResultSet rs = query.select("airport");

            //  Con la seguente istruzione si sta realizzando una lista di handler del tipo String
            //  questi handler vengono utilizzati per referenziare le informazioni recuperate con il suddetto SELECT

            ArrayList<String> tempList = new ArrayList<String>();

            //  Il seguente ciclo while consente di scorrere le tuple restituite dal suddetto SELECT,
            //      Dei dati contenuti all'interno della tupla si recupera soltanto la nazione.

            //  ad ogni iterazione aumenta il numero di handler presenti nell'Array. Ogni handler introdotto
            //  viene utilizzato per referenziare la nazione che figura nella tupla in esame

            while (rs.next()) {
                tempList.add(rs.getString("country"));
            }

            //  Dato che ogni nazione figura in più tuple, si utilizza il metodo distinct().collect(Collectors.toList())
            //  per creare un ArrayList privo di duplicati. In questo modo la stessa stringa non risulta essere allocata più volte

            ArrayList<String> countryList = (ArrayList<String>) tempList.stream().distinct().collect(Collectors.toList());

            //  La classe FXCollections fornisce un metodo per creare un'ObservableList a partire da un'ArrayList. Il metodo
            //  in questione è observableArrayList(countryList). Ultimata la creazione la lista viene associata ai due ComboBox
            //  che figurano nella pagina per aggiungere richieste al database.

            CountryBoxList = FXCollections.observableArrayList(countryList);
            fromCountryBox.setItems(CountryBoxList);
            toCountryBox.setItems(CountryBoxList);

            //  Le seguenti istruzioni consentono di l'handler che deve gestire l'evento 'OnAction', ovvero l'evento
            //  che si genera nell'istante in cui si utilizzano:
            //      i button '+' e '-'
            //      i checkbox

            adultsPlusButton.setOnAction(ehPlusMinusButton);
            adultsMinusButton.setOnAction(ehPlusMinusButton);
            childrenMinusButton.setOnAction(ehPlusMinusButton);
            childrenPlusButton.setOnAction(ehPlusMinusButton);

            lookForCheckBox.setOnAction(ehCheckOfferShare);
            offerCheckBox.setOnAction(ehCheckOfferShare);
            shareCheckBox.setOnAction(ehCheckOfferShare);


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //  Il metodo in questione viene eseguito nell'istante in cui si clicca sul pulsante 'Add Request'
    //  esso consente di aggiungere nuove richieste all'interno della table requests presente nel DB
    //  nuove richieste di offro / cerco / condivido passaggio

    @FXML
    private void addRequest() {

        //  Questa variabile viene settata a TRUE se nel database figura già una richiesta da parte dell'utente X per il giorno Y all'ora Z

        boolean exist = false;

        //  Con le seguenti istruzioni vengono istanziati in memoria degli oggetti del tipo String. Questi oggetti vengono
        //  inizializzati con i dati introdotti nei campi che figurano nella scena

        String fromAirport = (String) fromAirportBox.getSelectionModel().getSelectedItem();
        String toAirport = (String) toAirportBox.getSelectionModel().getSelectedItem();
        String fromCountry = (String) fromCountryBox.getSelectionModel().getSelectedItem();
        String toCountry = (String) toCountryBox.getSelectionModel().getSelectedItem();
        String street = streetField.getText();
        String town = townField.getText();
        String serviceType = null;


        //  Se all'invio della richiesta il checkbox attivo è "condivido passaggio"
        //      e se oltre ad esso è attivo il checkbox associato a "cerco passaggio", allora all'interno della tabella requests nel campo serviceType
        //      figurerà la dicitura 'shareAndlook';

        //      e se esso è l'unico checkbox attivo, allora all'interno della tabella requests nel campo serviceType
        //      figurerà la dicitura'share';

        if (shareCheckBox.isSelected()) {

            if (lookForCheckBox.isSelected())
                serviceType = "shareAndlook";

            else
                serviceType = "share";

        }


        //  Se l'unico checkbox attivo è "cerco passaggio"
        //  all'interno della tabella requests nel campo serviceType dovrà figurare la dicitura 'look';

        else if (lookForCheckBox.isSelected() && !shareCheckBox.isSelected())
            serviceType = "look";

            //  Se l'unico checkbox attivo è "offro passaggio"
            //   all'interno della tabella requests nel campo serviceType dovrà figurare la dicitura 'offer';

        else if (offerCheckBox.isSelected())
            serviceType = "offer";


        //  Il seguente costrutto selettivo consente di verificare se sono stati lasciati dei campi vuoti

        if (fromCountry != null && toCountry != null && fromAirport != null && toAirport != null && !fromAirport.matches(toAirport) && !street.isEmpty() && !town.isEmpty() && serviceType != null) {

            //  Se tutti i campi sono stati compilati, i loro contenuto viene allocato in memoria e referenziato con l'ausilio di 'n' handler
            //  questi n-handler del tipo String fanno parte dell'ArrayList istanziato con la seguente istruzione.

            ArrayList<String> value = new ArrayList<>();
            value.add(fromCountry);
            value.add(toCountry);
            value.add(fromAirport);
            value.add(toAirport);
            value.add(dateField.getValue().toString());
            value.add(timeField.getValue().toString());
            value.add(town);
            value.add("n1");
            value.add(adultsField.getText());
            value.add(childrenField.getText());
            value.add(serviceType);


            //  Con questa istruzione si definisce un handler MySQLQuery e si fa in modo che possa referenziare l'unica istanza di questa classe

            MySQLQuery query = MySQLQuery.getInstance();

            //  Con questa istruzione si esegue un SELECT sulla tabella requests

            ResultSet rs = query.select("requests");

            try {


                //  Il seguente ciclo while consente di analizzare le tuple restituite dall'istruzione di SELECT. Ad ogni iterazione si verifica se i dati presenti nei campi:
                //  author, arriveTime e arriveDate coincidono o meno con quelli appena forniti. Se questi tre parametri coincidono con i dati che figurano nella richiesta
                //  che l'utente sta tentando di aggiungere al database, la variabile 'exists' viene settata a TRUE e si esce dal ciclo while.

                while (rs.next()) {

                    if (rs.getString("author").matches(value.get(6)) && rs.getString("arriveTime").matches(value.get(3)) && rs.getString(("arriveDate")).matches(value.get(2))) {
                        exist = true;
                        break;
                    }
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            //  Se con il suddetto ciclo while non sono state individuate corrispondenze tra la richieste aperte e quella che si sta tentando di aprire,
            //  si esegue un INSERT e si carica la pagina che segnala che la richiesta è stata inoltrata con successo al database

            if (!exist) {

                Map obj = Map.getInstance();

                value.add(obj.adjustAddress(street,town,"address"));

                try {
                    query.insert("requests", value, "requests");

                } catch (SQLException e) {

                    e.printStackTrace();

                }

                mainApp.initOutcomeAddRequestWindow("done");
                System.out.println("La richiesta è stata inoltrata con successo!");
            }

            //  Se con il suddetto ciclo while è stata individuata una corrispondenza tra i dati forniti e quelli presenti nel database
            //  l'INSERT non viene eseguito e viene caricata la scena che segnala che l'operazione non ha avuto esito positivo

            else {

                mainApp.initOutcomeAddRequestWindow("failed");
                System.out.println("Hai già inoltrato questa richiesta!");

            }
        }

        //  Se nell'istante in cui si clicca sul pulsante 'Add Request' vi sono dei campi vuoti viene eseguita l'istruzione associata all'else
        //  ossia viene caricata la scena che segnala che l'operazione non ha avuto esito positivo in quanto il form non è stato compilato correttamente / completamente

        else {
            mainApp.initOutcomeAddRequestWindow("failed");
            System.out.println("Bisogna compilare tutti i campi prima d'inoltrare la richiesta!");
        }
    }

    //  I metodi che seguono vengono eseguiti nell'istante in cui si sceglie una Nazione.

    //  Il primo fa in modo che nel menù a tendina 'From Airport' figurino solo gli aeroporti della nazione selezionata
    //  dal menù a tendina 'From Country'

    //  Il secondo fa in modo che nel menù a tendina 'To Airport' figurino solo gli aeroporti della nazione selezionata
    //  dal menù a tendina 'To Country'

    @FXML
    private void sortFromAirportList() {


        if (fromCountryBox.getSelectionModel().getSelectedItem() != null) {

            try {

                MySQLQuery query = MySQLQuery.getInstance();
                ResultSet rs;

                String country = (String) fromCountryBox.getSelectionModel().getSelectedItem();
                rs = query.select("airport");

                ArrayList<String> list = new ArrayList<String>();

                while (rs.next()) {

                    if (rs.getString(2).matches(country))
                        list.add(rs.getString(1));
                }


                fromAirportBoxList = FXCollections.observableArrayList(list);
                fromAirportBox.setItems(fromAirportBoxList);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void sortToAirportList() {


        if (toCountryBox.getSelectionModel().getSelectedItem() != null) {
            try {

                MySQLQuery query = MySQLQuery.getInstance();
                ResultSet rs;

                String country = (String) toCountryBox.getSelectionModel().getSelectedItem();
                rs = query.select("airport");

                ArrayList<String> list = new ArrayList<String>();

                while (rs.next()) {

                    if (rs.getString(2).matches(country))
                        list.add(rs.getString(1));
                }

                toAirportBoxList = FXCollections.observableArrayList(list);
                toAirportBox.setItems(toAirportBoxList);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    //  I metodi che seguono consentono d'incrementare e decrementare il valore che figura nei TextField contenenti
    //  il numero di adulti e il numero di bambini.

    //  Il primo consente d'incrementare il valore. Questo viene evocato dall'handler 'ehPlusMinusButton' quando il
    //  si clicca sul button '+';

    //  Il secondo consente di decrementare il valore, fino a portarlo a zero. Questo viene evocato dall'handler
    //  'ehPlusMinusButton' quando si clicca sul button '-';

    @FXML
    private void increment(String id) {

        if (id.matches("adultsField")) {
            int num = Integer.parseInt(adultsField.getText());
            adultsField.setText(new Integer(++num).toString());
        } else {
            int num = Integer.parseInt(childrenField.getText());
            childrenField.setText(new Integer(++num).toString());
        }
    }

    @FXML
    private void decrement(String id) {


        if (id.matches("adultsField")) {
            int num = Integer.parseInt(adultsField.getText());

            if (num > 0)
                adultsField.setText(new Integer(--num).toString());
        } else {

            int num = Integer.parseInt(childrenField.getText());

            if (num > 0)
                childrenField.setText(new Integer(--num).toString());
        }
    }

    //  Quelli che seguono sono i due event handler che gestiscono l'evento 'OnAction' generato nell'istante in cui
    //  s'interagisce o con i button '+' e '-', oppure con i checkbox.
    //  N.B. sia ehPlusMinusButton che ehCheckOfferShare referenziano un'istanza di una classe anonima

    EventHandler ehPlusMinusButton = new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent event) {

            if (event.getSource() instanceof JFXButton) {

                if (((JFXButton) event.getSource()).getId().matches("adultsPlusButton"))
                    increment("adultsField");

                else if (((JFXButton) event.getSource()).getId().matches("childrenPlusButton"))
                    increment("childrenField");

                else if (((JFXButton) event.getSource()).getId().matches("adultsMinusButton"))
                    decrement("adultsField");

                else if (((JFXButton) event.getSource()).getId().matches("childrenMinusButton"))
                    decrement("childrenField");

            }
        }
    };

    EventHandler ehCheckOfferShare = new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent event) {

            if (event.getSource() instanceof JFXCheckBox) {

                JFXCheckBox eh = (JFXCheckBox) event.getSource();

                if (eh.getId().matches("offerCheckBox")) {

                    if (lookForCheckBox.isSelected())
                        lookForCheckBox.setSelected(false);

                    if (offerCheckBox.isSelected())
                        shareCheckBox.setSelected(false);
                } else if ((eh.getId().matches("lookForCheckBox") || eh.getId().matches("shareCheckBox")) && offerCheckBox.isSelected())
                    offerCheckBox.setSelected(false);


            }
        }
    };
}

