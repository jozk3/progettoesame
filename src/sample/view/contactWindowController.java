package sample.view;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.stage.Stage;
import sample.MainApp;
import sample.model.MySQLQuery;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class contactWindowController {

    @FXML
    JFXTextField toField;
    @FXML
    JFXTextField objectField;
    @FXML
    JFXTextArea messageTextArea;
    @FXML
    JFXButton sendButton;




    private MainApp mainApp;

    EventHandler closeContactStageHandler = new EventHandler<ActionEvent>() {
        /**
         * Invoked when a specific event of the type for which this handler is
         * registered happens.
         *
         * @param event the event which occurred
         */
        @Override
        public void handle(ActionEvent event) {

            sendMessage();
            JFXButton button = (JFXButton) event.getSource();
            Stage contactWindowStage = (Stage) button.getScene().getWindow();
            contactWindowStage.close();
        }
    };

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    public void setRecipient(String name) {
        toField.setText(name);
    }

    public void initialize() {
        sendButton.setOnAction(closeContactStageHandler);
    }

    private void sendMessage() {

        ArrayList<String> field = new ArrayList<String>();
        ArrayList<String> value = new ArrayList<String>();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        MySQLQuery query = MySQLQuery.getInstance();

        field.add("author");
        value.add(mainApp.getUserID());
        field.add("recipient");
        value.add(toField.getText());
        field.add("dateAndTime");
        value.add(dtf.format(now));
        field.add("text");
        value.add(messageTextArea.getText());
        field.add("type");
        value.add("new");

        try {
            query.insert("messages", value, "newMessage");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
