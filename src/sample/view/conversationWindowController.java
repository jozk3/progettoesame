package sample.view;

import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import sample.MainApp;
import sample.model.MySQLQuery;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

public class conversationWindowController {

    @FXML
    JFXListView conversationListView;

    @FXML
    Label titleField;


    private MainApp mainApp;

    String user;

    public void setUser(String user){this.user = user;}

    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
    }

    public void addMessage(ArrayList<String> authors, ArrayList<String> messages){

        try {

            int num = authors.size();

            for(int i = 0; i < num; i++) {

                FXMLLoader messageLoader = new FXMLLoader();

                if (authors.get(i).matches(mainApp.getUserID()))
                    messageLoader.setLocation(getClass().getResource("messageAnchorPane2.fxml"));
                else
                    messageLoader.setLocation(getClass().getResource("messageAnchorPane.fxml"));

                AnchorPane messageAnchorPane = (AnchorPane) messageLoader.load();

                int numChildren = messageAnchorPane.getChildren().size();

                for (int j = 0; j < numChildren; j++) {

                    if (messageAnchorPane.getChildren().get(j).getId() != null && messageAnchorPane.getChildren().get(j).getId().matches("messageArea")) {

                        JFXTextArea messageArea = (JFXTextArea) messageAnchorPane.getChildren().get(j);
                        messageArea.setText(messages.get(i));
                    }

                    else if (messageAnchorPane.getChildren().get(j).getId() != null && messageAnchorPane.getChildren().get(j).getId().matches("authorMessageField")) {

                        JFXTextField authorField = (JFXTextField) messageAnchorPane.getChildren().get(j);
                        authorField.setText(authors.get(i) + ":");
                        break;
                    }
                }

                conversationListView.getItems().add(messageAnchorPane);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void removeAllMessages(){
        conversationListView.getItems().clear();
    }

    @FXML
    private void replay(){
        mainApp.initContactWindow(user);
    }


}








