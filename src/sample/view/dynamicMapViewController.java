package sample.view;

import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import sample.MainApp;

public class dynamicMapViewController {

    private MainApp mainApp;

    @FXML
    WebView mapWebView;


    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
    }


    public void loadMap(String startAddress, String startTown, String endAddress, String endTown){

        String url = "http://www.mapquest.com/embed?saddr=" + startAddress + " " + startTown +
                "&daddr=" + endAddress + " " + endTown + "&maptype=map";

        System.out.println(url);

        /*String url = "http://www.mapquest.com/embed?saddr=Via Ceraselle Caianello &daddr=Cassino FR&maptype=map";*/

        WebEngine engine = mapWebView.getEngine();
        engine.load(url);
    }


}
