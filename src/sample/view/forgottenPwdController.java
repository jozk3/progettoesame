package sample.view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import sample.MainApp;
import sample.model.MailSystem;
import sample.model.Users;
import sample.model.MySQLQuery;
import sample.model.dao.DAOException;

import javax.mail.MessagingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;



public class forgottenPwdController {


    //L'attributo in questione consente al controller di richiamare i metodi definiti nella classe "principale", ovvero la classe eseguibile
    private MainApp mainApp;

    //Il metodo in questione associa il suddetto handler all'unica istanza della classe. Istanza che viene creata
    //nell'istante in cui si esegue l'applicazione
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }


    //*-----------------Elementi che figurano nella scena realizzata con sceneBuilder-------------------*

    @FXML
    TextField forgottenUsrField;

    @FXML
    TextField forgottenMailField;

    @FXML
    Button forgottenSendButton;

    //*-------------------------------------------------------------------------------------------------------*


    //  Il metodo in questione viene eseguito nell'istante in cui si clicca sul pulsante 'Reset Password' che figura
    //  nella finestra accessibile tramite il link 'Forgotten Password' che figura nella finestra di login

    @FXML
    private void handleForgottenPwd(){


        try {

            //Viene istanziato un oggetto del tipo Users e viene inizializzato soltanto uno dei suoi attribuiti
            //L'attributo inizializzato è quello destinato ad ospitare lo username dell'utente
            Users user = new Users();
            user.setUsr(forgottenUsrField.getText());

            //Con questa istruzione si definisce un handler MySQLQuery e si fa in modo che possa referenziare l'unica istanza di questa classe
            MySQLQuery query = MySQLQuery.getInstance();

            //Con questa istruzione si esegue un SELECT sulla tabella users
            ResultSet rs = query.select("users");

            //Con questa istruzione si realizza un Array di handler del tipo Users, ogni handler referenzierà un'istanta della classe
            //istanza che viene allocata all'interno del ciclo while che segue
            ArrayList<Users> users = new ArrayList<Users>();


            //Il ciclo while consente di analizzare per ogni tupla il valore contenuto nei campi indicati di seguito
            //Il valore di questi campi viene utilizzato per inizializzare gli attributi dell'oggetto Users istanziato
            //Al termine, l'oggetto viene fatto referenziare da uno degli handler del suddetto ArrayList

            while(rs.next()){

                Users a = new Users();
                a.setUsr(rs.getString("username"));
                a.setPassword(rs.getString("password"));
                a.setMail(rs.getString("mail"));
                a.setPhone(rs.getString("phone"));
                a.setName(rs.getString("name"));
                a.setSurname(rs.getString("surname"));
                users.add(a);
            }


            //Il seguente ciclo for consente di scorrere il suddetto array, esso serve per individuare l'utente che ha richiesto un reset della password
            //Se l'utente viene individuato:
                //viene evocato il metodo che sostituisce la scena contenente la pagina di recupero password con la scena contenente la pagina di login
                //viene evocato il metodo che effettua il reset della password
                //si esce dal ciclo for

            for(int i = 0; i < users.size(); i++){

                if(users.get(i).getUsr().matches(forgottenUsrField.getText()) && users.get(i).getMail().matches(forgottenMailField.getText())){
                    mainApp.initLoginWindow();
                    restorePwd();
                    break;
                }

                else{
                    mainApp.initLoginWindow();
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //  Il metodo in questione viene eseguito nell'istante in cui si clicca sul pulsante 'Login'
    //  esso consente di tornare alla schermata di login

    @FXML
    private void showLoginWindow(){
        mainApp.initLoginWindow();
    }

    //  Il metodo in questione con l'ausilio di un generatore di numeri casuali, genera una password
    //  La password generata viene inviata tramite email all'utente che ha richiesto il reset della password
    //  e viene inserita nel database al posto della precedente

    private void restorePwd(){

        //  Con la seguente istruzione si va ad istanziare in memoria un oggetto del tipo Integer
        //  l'unico attributo (Del tipo int) dell'oggetto in questione viene inizializzato con una sequenza casuale di numeri interi
        //  La sequenza viene successivamente convertira in una stringa. L'Unsigned è necessario per via del fatto che
        //  il generatore in alcuni casi produce in uscita una sequenza di numeri negativi
        //  La sequenza dev'essere convertita in una stringa per via del fatto che il campo 'Password' nella table 'users' accetta solo
        //  VARCHAR()

        Integer rand = new Integer(ThreadLocalRandom.current().nextInt());
        String sequence = Integer.toUnsignedString(rand);

        //  Con questa istruzione si definisce un handler MySQLQuery e si fa in modo che possa referenziare l'unica istanza di questa classe

        MySQLQuery user = MySQLQuery.getInstance();

        //  Con le seguenti istruzioni si alloca in memoria un oggetto del tipo Users e s'inizializza il suo attributo 'username'
        //  con il testo introdotto dall'utente nel JFoenix TextField

        //Users a = new Users();
        //a.setUsr(forgottenUsrField.getText());

        //  Si definisce un ArrayList da un unico elemento per via del fatto che il campo d'aggiornare è unico
        //  Si utilizza proprio un ArrayList per via del fatto che il metodo che effettua l'update accetta come
        //  parametri in ingresso solo ArrayList

        ArrayList<String> c = new ArrayList<String>(Arrays.asList("username","password"));
        ArrayList<String> v = new ArrayList<String>(Arrays.asList(forgottenUsrField.getText(),sequence));


            //  Con le sequenti istruzioni:

            //  si effettua l'update, ovvero si va a sostituire la password dimenticata con quella prodotta dal generatore
            //  di numeri di casuali;

            //  s'invia un'email all'utente con la quale gli si comunica la nuova password;

            user.update("users", c, v);
            Thread prova = new Thread() {

                @Override
                public void run() {
                    try {
                        MailSystem.sendEmail("smtp.libero.it", "25", "francescabianchiing@libero.it", "Francescabianchi1@",
                                forgottenMailField.getText(), "RECUPERO PASSWORD", "Gentile " + forgottenUsrField.getText() + "\n" +
                                        "\n" +
                                        "La sua nuova Password è: " + sequence + "\n" +
                                        "\n" +
                                        "I migliori saluti,\n" +
                                        "Servizio Clienti MeetFly ");
                    } catch (MessagingException e) {
                        e.printStackTrace();
                    }
                }

            };

            prova.start();
        }
}
