package sample.view;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import sample.MainApp;
import sample.model.Users;
import sample.model.MySQLQuery;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.ArrayList;


public class loginController implements Initializable {

    //*-----------------Elementi che figurano nella scena realizzata con sceneBuilder-------------------*

    @FXML
    private JFXButton registerButton;

    @FXML
    private JFXButton loginButton;

    @FXML
    private JFXTextField usrLoginField;

    @FXML
    private JFXPasswordField pwdLoginField;

    @FXML
    private Label invalidDataText;

    @FXML
    private JFXButton forgottenPassText;

    //*-------------------------------------------------------------------------------------------------------*


    //  L'attributo in questione consente al controller di richiamare i metodi definiti nella classe "principale", ovvero la classe eseguibile

    private MainApp mainApp;

    //  Il metodo in questione associa il suddetto handler all'unica istanza della classe. Istanza che viene creata
    //  nell'istante in cui si esegue l'applicazione

    public void setMainApp(MainApp mainApp){

        this.mainApp = mainApp;
    }


    //  Il metodo in questione viene eseguito in maniera automatica nell'istante in cui viene istanziato un oggetto di questa classe
    //  ciò accade quando viene eseguito il metodo initLoginWindow

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //  Attivano l'animazione dei JFoenix TextField che figurano nella schermata di Login
        usrLoginField.setLabelFloat(true);
        pwdLoginField.setLabelFloat(true);

        //  Rende invisibile la scritta rossa che indica che i dati inseriti per il login sono errati
        invalidDataText.setVisible(false);

    }


    //  Il metodo in questione viene eseguito nell'istante in cui si fa click sul tasto login che figura nella
    //  schermata di Login

    @FXML
    private void handleLogin(){

        boolean checkDate = false;

        //  Con questa istruzione si definisce un handler MySQLQuery e si fa in modo che possa referenziare l'unica istanza di questa classe
        MySQLQuery sql = MySQLQuery.getInstance();


        try {

            //  Con questa istruzione si esegue un SELECT sulla tabella users

            ResultSet rs = sql.select("users");

            //  The ArrayList class is a resizable array, which can be found in the java.util package.

            //  The difference between a built-in array and an ArrayList in Java, is that the size of an array cannot be modified
            //  (if you want to add or remove elements to/from an array, you have to create a new one).
            //  While elements can be added and removed from an ArrayList whenever you want.


            ArrayList<Users> users = new ArrayList<Users>();


            //  Il seguente ciclo while consente di accedere ai dati contenuti nelle tuple restituite dal SELECT
            //  Al suo interno viene istanziato un oggetto della classe Users, i suoi attributi vengono inizializzati
            //  e successivamente l'oggetto viene inserito all'interno del suddetto array

            while(rs.next()){

                Users a = new Users();
                a.setUsr(rs.getString("username"));
                a.setPassword(rs.getString("password"));
                a.setMail(rs.getString("mail"));
                a.setPhone(rs.getString("phone"));
                a.setName(rs.getString("name"));
                a.setSurname(rs.getString("surname"));
                users.add(a);
            }

            //  Con il seguente ciclo for si verifica se esiste un utente con il nickname specificato nel JFoenix TextField
            //  successivamente si verifica se la password di questo utente coincide con quella specifita nel JFoenix PasswordField
            //  Se ciò accade il valore della variabile checkDate viene aggiornato con true

            Users user = users.get(0);

            for(int i=1 ; i < users.size(); i++){

                if(user.getUsr().matches(usrLoginField.getText()) && user.getPassword().matches(pwdLoginField.getText()))
                    checkDate = true;

                user = users.get(i);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        //  Se il controllo ha avuto esito positivo, si utilizza l'handler mainApp per richiamare il metodo che provvederà a
        //  caricare nello stage la schermata principale dell'applicazione e ad associare l'handler 'userID' definito nella classe Mainapp
        //  all'oggetto del tipo String istanziato alla chiamata del metodo 'getText()'. Questo oggetto conterrà il nickname inserito nel
        //  JFoenix TextFild.

        //  In caso contrario si rende visibile il testo che indica che i dati introdotti sono errati.

        if(checkDate) {
            mainApp.setUserID(usrLoginField.getText());
            mainApp.initMainWindow();
        }

       else
           invalidDataText.setVisible(true);
    }

    //  Il metodo in questione viene eseguito nell'istante in cui si fa click sul tasto Sign Up che figura nella
    //  schermata di Login. Esso consente di richiamare il metodo che provvede a caricare nello stage la schermata di registrazione

    @FXML
    private void showRegisterWindow(){
        mainApp.initRegister();
    }

    //  Il metodo in questione viene eseguito nell'istante in cui si fa click sulla Label Forgotten Password che figura nella
    //  schermata di Login. Esso consente di richiamare il metodo che provvede a caricare nello stage la schermata di recupero password

    @FXML
    private void showForgottenPwdWindow() {
        mainApp.initForgottenPwd();
    }


}
