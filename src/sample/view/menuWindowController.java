package sample.view;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import sample.MainApp;

import java.io.File;

public class menuWindowController {

    //L'attributo in questione consente al controller di richiamare i metodi definiti nella classe "principale", ovvero la classe eseguibile
    private MainApp mainApp;


    //Il metodo in questione associa il suddetto handler all'unica istanza della classe. Istanza che viene creata
    //nell'istante in cui si esegue l'applicazione

    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
    }



    //*-----------------Elementi che figurano nella scena realizzata con sceneBuilder-------------------*

    @FXML
    JFXButton addRequestButton;

    @FXML
    Text loggedUserText;

    @FXML
    ImageView avatar;

    //*-------------------------------------------------------------------------------------------------------*

    //Il metodo in questione sfrutta l'handler mainApp per aggiornare la scena rappresentata nella parte centrale
    //del rootLayout. La parte centrale in questo caso viene aggiornata con la schermata che consente di aggiungere
    //richieste al database

    @FXML
    private void showAddRequestWindow(){
        mainApp.initAddRequest();
    }

    //Il metodo in questione sfrutta l'handler mainApp per aggiornare la scena rappresentata nella parte centrale
    //del rootLayout. La parte centrale in questo caso viene aggiornata con la schermata di profilo

    @FXML
    private void showProfileWindow(){
        mainApp.initProfileWindow();
    }

    @FXML
    private void showSearchWindow(){mainApp.initSearchRequestWindow();}

    @FXML
    private void showTestWindow(){
        mainApp.prova();
    }

    @FXML
    private void logOutButton(){
        mainApp.exit();
    }

    @FXML
    private void showMessagesWindow(){mainApp.initMessagesWindow();}

    public void setLoggedUser(){

        loggedUserText.setText(mainApp.getUserID());
        /*File file = new File("prova.gif");
        Image image = new Image(file.toURI().toString());
        avatar.setImage(image);*/
    }



}
