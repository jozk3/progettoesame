package sample.view;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import org.apache.commons.lang3.StringUtils;
import sample.MainApp;
import sample.model.MySQLQuery;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static sample.MainApp.downloadFile;


public class myRequestsWindow {



    //*-----------------Elementi che figurano nella scena realizzata con sceneBuilder-------------------*

    @FXML
    JFXListView testView;

    @FXML
    JFXTextField nickNameField;

    @FXML
    JFXTextField toAirportField;

    @FXML
    JFXTextField destinationField;

    @FXML
    JFXTextField dateTimeField;

    @FXML
    JFXTextField typeSharingField;

    @FXML
    JFXTextField passengersField;

    @FXML
    JFXButton deleteButton;

    @FXML
    JFXButton contactButton;

    @FXML
    JFXButton viewMapButton;

    //*-------------------------------------------------------------------------------------------------------*

    //  L'attributo in questione consente al controller di richiamare i metodi definiti nella classe "principale", ovvero la classe eseguibile

    private MainApp mainApp;

    private String myDestination;
    private String myTown;

    //  Il metodo in questione associa il suddetto handler all'unica istanza della classe. Istanza che viene creata
    //  nell'istante in cui si esegue l'applicazione

    public void setMainApp (MainApp mainApp){
        this.mainApp = mainApp;
    }

    public void setMyDestination (String myDestination){
        this.myDestination = myDestination;
    }

    public void setMyTown(String myTown){
        this.myTown = myTown;
    }


    //  Il metodo in questione viene richiamato all'interno del metodo "prova()",
    //  questo metodo viene eseguito nell'istante in cui si fa click sul button
    //  'My Requests's List' che figura nel Menù Laterale

    public void showAllMyRequests(){

        MySQLQuery query = MySQLQuery.getInstance();

        ResultSet rs = query.select("requests");

        int j = 0;

        try {
        while(rs.next()){

            FXMLLoader requestPaneLoader = new FXMLLoader();
            if(j % 2 == 0)
                requestPaneLoader.setLocation(mainApp.getClass().getResource("view/myRequestWindow2.fxml"));
            else
                requestPaneLoader.setLocation(mainApp.getClass().getResource("view/myRequestWindow.fxml"));
            AnchorPane requestAnchorPane= (AnchorPane) requestPaneLoader.load();
            int num = requestAnchorPane.getChildren().size();
            boolean entered = false;
            j++;

            for(int i = 0; i < num; i++){

                if(rs.getString("author").matches(mainApp.getUserID())) {
                    entered = true;
                    if (requestAnchorPane.getChildren().get(i).getId() != null && requestAnchorPane.getChildren().get(i).getId().matches("nickNameField")) {
                        JFXTextField textField = (JFXTextField) requestAnchorPane.getChildren().get(i);
                        textField.setText(rs.getString("author"));
                    }

                    if (requestAnchorPane.getChildren().get(i).getId() != null && requestAnchorPane.getChildren().get(i).getId().matches("toCountryField")) {
                        JFXTextField textField = (JFXTextField) requestAnchorPane.getChildren().get(i);
                        textField.setText(rs.getString("toCountry"));
                    }


                    if (requestAnchorPane.getChildren().get(i).getId() != null && requestAnchorPane.getChildren().get(i).getId().matches("passengersField")) {
                        JFXTextField textField = (JFXTextField) requestAnchorPane.getChildren().get(i);
                        textField.setText(Integer.toString(Integer.parseInt(rs.getString("adultsNumber")) + Integer.parseInt(rs.getString("childrenNumber"))));
                    }

                    if (requestAnchorPane.getChildren().get(i).getId() != null && requestAnchorPane.getChildren().get(i).getId().matches("toAirportField")) {
                        JFXTextField textField = (JFXTextField) requestAnchorPane.getChildren().get(i);
                        textField.setText(rs.getString("toAirport"));
                    }


                    if (requestAnchorPane.getChildren().get(i).getId() != null && requestAnchorPane.getChildren().get(i).getId().matches("dateTimeField")) {
                        JFXTextField textField = (JFXTextField) requestAnchorPane.getChildren().get(i);
                        textField.setText(rs.getString("arriveDate") + ", " + rs.getString("arriveTime"));
                    }

                    if (requestAnchorPane.getChildren().get(i).getId() != null && requestAnchorPane.getChildren().get(i).getId().matches("deleteButton")) {
                        JFXButton button = (JFXButton) requestAnchorPane.getChildren().get(i);
                        button.setOnAction(deleteHandler);
                    }

                    if (requestAnchorPane.getChildren().get(i).getId() != null && requestAnchorPane.getChildren().get(i).getId().matches("typeSharingField")) {
                        JFXTextField textField = (JFXTextField) requestAnchorPane.getChildren().get(i);
                        textField.setText(rs.getString("serviceType"));
                    }

                    if (requestAnchorPane.getChildren().get(i).getId() != null && requestAnchorPane.getChildren().get(i).getId().matches("destinationField")) {
                        JFXTextField textField = (JFXTextField) requestAnchorPane.getChildren().get(i);
                        textField.setText(rs.getString("address") + ", " + rs.getString("town"));
                    }
                }

            }

            if(entered){
                testView.getItems().add(requestAnchorPane);
                entered = false;
            }

        }}catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    //  Il metodo in questione viene richiamato all'interno del metodo "initResultsSearchWindow(...)",
    //  questo metodo viene eseguito nell'istante in cui si fa click sul button
    //  'Search' che figura nella schermata accessibile mediante il button 'Search Requests'
    //  che figura nel Menù Laterale

    public void showAllRequests(ResultSet searchResult){


        ResultSet rs = searchResult;

        int j = 0;

        try {
            while(rs.next()){

                FXMLLoader requestPaneLoader = new FXMLLoader();
                if(j % 2 == 0)
                    requestPaneLoader.setLocation(mainApp.getClass().getResource("view/requestWindow2.fxml"));
                else
                    requestPaneLoader.setLocation(mainApp.getClass().getResource("view/requestWindow.fxml"));
                AnchorPane requestAnchorPane= (AnchorPane) requestPaneLoader.load();
                boolean entered = false;
                int num = requestAnchorPane.getChildren().size();
                j++;

                for(int i = 0; i < num; i++) {

                    if (!rs.getString("author").matches(mainApp.getUserID())) {

                        if(!entered)
                            entered = true;

                        if (requestAnchorPane.getChildren().get(i).getId() != null && requestAnchorPane.getChildren().get(i).getId().matches("nickNameField")) {
                            JFXTextField textField = (JFXTextField) requestAnchorPane.getChildren().get(i);
                            textField.setText(rs.getString("author"));

                        }

                        if (requestAnchorPane.getChildren().get(i).getId() != null && requestAnchorPane.getChildren().get(i).getId().matches("toCountryField")) {
                            JFXTextField textField = (JFXTextField) requestAnchorPane.getChildren().get(i);
                            textField.setText(rs.getString("toCountry"));
                        }


                        if (requestAnchorPane.getChildren().get(i).getId() != null && requestAnchorPane.getChildren().get(i).getId().matches("passengersField")) {
                            JFXTextField textField = (JFXTextField) requestAnchorPane.getChildren().get(i);
                            textField.setText(Integer.toString(Integer.parseInt(rs.getString("adultsNumber")) + Integer.parseInt(rs.getString("childrenNumber"))));
                        }

                        if (requestAnchorPane.getChildren().get(i).getId() != null && requestAnchorPane.getChildren().get(i).getId().matches("toAirportField")) {
                            JFXTextField textField = (JFXTextField) requestAnchorPane.getChildren().get(i);
                            textField.setText(rs.getString("toAirport"));
                        }


                        if (requestAnchorPane.getChildren().get(i).getId() != null && requestAnchorPane.getChildren().get(i).getId().matches("dateTimeField")) {
                            JFXTextField textField = (JFXTextField) requestAnchorPane.getChildren().get(i);
                            textField.setText(rs.getString("arriveDate") + ", " + rs.getString("arriveTime"));
                        }

                        if (requestAnchorPane.getChildren().get(i).getId() != null && requestAnchorPane.getChildren().get(i).getId().matches("contactButton")) {
                            JFXButton button = (JFXButton) requestAnchorPane.getChildren().get(i);
                            button.setOnAction(contactHandler);
                        }

                        if (requestAnchorPane.getChildren().get(i).getId() != null && requestAnchorPane.getChildren().get(i).getId().matches("viewMapButton")) {
                            JFXButton button = (JFXButton) requestAnchorPane.getChildren().get(i);
                            button.setOnAction(viewMap);
                        }

                        if (requestAnchorPane.getChildren().get(i).getId() != null && requestAnchorPane.getChildren().get(i).getId().matches("typeSharingField")) {
                            JFXTextField textField = (JFXTextField) requestAnchorPane.getChildren().get(i);
                            textField.setText(rs.getString("serviceType"));
                        }

                        if (requestAnchorPane.getChildren().get(i).getId() != null && requestAnchorPane.getChildren().get(i).getId().matches("destinationField")) {
                            JFXTextField textField = (JFXTextField) requestAnchorPane.getChildren().get(i);
                            textField.setText(rs.getString("address") + ", " + rs.getString("town"));
                        }
                    }
                }

                if(entered){
                    entered = false;
                    testView.getItems().add(requestAnchorPane);
                }


            }}catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    //  Quello che segue è l'handler dell'evento 'onAction' generato dai pulsanti 'Delete' che figurano
    //  negli item della ListView presente all'interno della pagina 'My Requests's List'. Il suddetto evento
    //  viene generato nell'istante in cui si fa click su uno dei button 'Delete' presenti nella ListView

    EventHandler deleteHandler = new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent event) {


            //  Si definisce un handler del tipo JFXButton e lo si utilizza per referenziare il nodo che ha
            //  generato l'evento 'onAction'.

            JFXButton button = (JFXButton) event.getSource();

            //  Si definisce un handler del tipo AnchorPane e lo si utilizza per referenziare il 'Parent Node' del
            //  button 'Delete', ovvero l'AnchorPane nel quale è contenuto il button cliccato

            AnchorPane anchorPane = (AnchorPane) button.getParent();

            
            JFXTextField textField;
            ArrayList<String> c = new ArrayList<String>();
            ArrayList<String> v = new ArrayList<String>();

            MySQLQuery query = MySQLQuery.getInstance();
            int num = anchorPane.getChildren().size();


            c.add("author");

            for(int i = 0; i < num; i++){

                if(anchorPane.getChildren().get(i).getId() != null && anchorPane.getChildren().get(i).getId().matches("nickNameField")){
                    textField = (JFXTextField) anchorPane.getChildren().get(i);
                    v.add(textField.getText());
                    break;
                }
            }


            c.add("arriveDate");

            for(int i = 0; i < num; i++){

                if(anchorPane.getChildren().get(i).getId() != null && anchorPane.getChildren().get(i).getId().matches("dateTimeField")){
                    textField = (JFXTextField) anchorPane.getChildren().get(i);
                    v.add(textField.getText().substring(0,textField.getText().indexOf(",")));
                    break;
                }
            }


            c.add("arriveTime");

            for(int i = 0; i < num; i++){

                if(anchorPane.getChildren().get(i).getId() != null && anchorPane.getChildren().get(i).getId().matches("dateTimeField")){
                    textField = (JFXTextField) anchorPane.getChildren().get(i);
                    v.add(textField.getText().substring(textField.getText().indexOf(",") + 2));
                    break;
                }
            }

            query.delete("requests" , c , v);

            testView.getItems().remove(button.getParent());
        }
    };

    EventHandler contactHandler = new EventHandler<ActionEvent>() {
        /**
         * Invoked when a specific event of the type for which this handler is
         * registered happens.
         *
         * @param event the event which occurred
         */
        @Override
        public void handle(ActionEvent event) {

            JFXButton contactButton = (JFXButton) event.getSource();
            AnchorPane itemAnchorPane = (AnchorPane) contactButton.getParent();
            int num = itemAnchorPane.getChildren().size();
            String name = null;

            for(int i = 0; i < num; i++){

                if(itemAnchorPane.getChildren().get(i).getId() != null && itemAnchorPane.getChildren().get(i).getId().matches("nickNameField")){

                    name = ((TextField) itemAnchorPane.getChildren().get(i)).getText();
                    break;
                }
            }

            mainApp.initContactWindow(name);

        }
    };

    EventHandler viewMap = new EventHandler<ActionEvent>() {
        /**
         * Invoked when a specific event of the type for which this handler is
         * registered happens.
         *
         * @param event the event which occurred
         */
        @Override
        public void handle(ActionEvent event) {

            JFXButton button = (JFXButton) event.getSource();
            AnchorPane buttonParent = (AnchorPane) button.getParent();
            int num = buttonParent.getChildren().size();
            String destination = myDestination;

            for(int i = 0; i < num; i++){

                if(buttonParent.getChildren().get(i).getId() != null && buttonParent.getChildren().get(i).getId().matches("destinationField")) {
                    JFXTextField textField = (JFXTextField) buttonParent.getChildren().get(i);
                    destination = textField.getText();
                }
            }

            String startAddress = destination.substring(0,destination.indexOf(","));
            String startTown = destination.substring(destination.indexOf(",")+2);
            String endAddress = myDestination;
            String endTown = myTown;

            /*String url2 = "https://www.mapquestapi.com/staticmap/v5/map?key=NgNSTQM96eAoTfKuam9DEB2AmeRxGymp&start=Cassino,FR|marker-A20000-D51A1A&end=" + destination + "|marker-008211-07e625&declutter=true&MA&traffic=flow|con|inc&format=gif&size=800,600@2x";
            url2 = StringUtils.deleteWhitespace(url2);

            try {
                downloadFile(new URL(url2),"IMG.gif");
            } catch (IOException e) {
                e.printStackTrace();
            }*/

            mainApp.initMapWindow(startAddress,startTown,myDestination,myTown);

        }
    };
}
