package sample.view;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;

import sample.MainApp;
import sample.model.MySQLQuery;
import sample.model.Users;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;


public class profileWindowController{

    private MainApp mainApp;
    private boolean editMode = false;
    private Users user = new Users();

    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
    }


    @FXML
    JFXTextField nameField;

    @FXML
    JFXTextField surnameField;

    @FXML
    JFXTextField usrField;

    @FXML
    JFXTextField phoneField;

    @FXML
    JFXTextField ageField;

    @FXML
    JFXTextField nationalityFiled;

    @FXML
    JFXTextField mailField;

    @FXML
    JFXPasswordField pwdField;

    @FXML
    JFXPasswordField rpwdField;

    @FXML
    JFXCheckBox fmaleCheckBox;

    @FXML
    JFXCheckBox maleCheckBox;

    @FXML
    JFXButton editSaveButton;


    @FXML
    private void editMode(){

        if(!editMode) {
            activeAllField();
            editMode = true;
            editSaveButton.setText("Save");
        }

        else{
            disableAllField();

            if(user.getUsr() == null)
                user.setUsr(mainApp.getUserID());

            user.setName(nameField.getText());
            user.setSurname(surnameField.getText());
            user.setPhone(phoneField.getText());

            if(maleCheckBox.isSelected())
                user.setSex("Male");
            else
                user.setSex("Fmale");

            user.setAge(ageField.getText());
            user.setMail(mailField.getText());
            user.setNationality(nationalityFiled.getText());
            user.setPassword(pwdField.getText());

            updatePersonalInformations();
            editSaveButton.setText("Modify");
        }
    }

    public void initField() {

        disableAllField();
        MySQLQuery query = MySQLQuery.getInstance();
        ResultSet rs = query.select("users");
        Users user = new Users();

        try{
            while(rs.next()){

                if(rs.getString("username").matches(mainApp.getUserID())) {
                    user.setName(rs.getString("name"));
                    user.setSurname(rs.getString("surname"));
                    user.setPhone(rs.getString("phone"));
                    user.setAge(rs.getString("age"));
                    user.setNationality(rs.getString("nationality"));
                    user.setSex(rs.getString("sex"));
                    user.setUsr(rs.getString("username"));
                    user.setMail(rs.getString("mail"));
                    user.setPassword(rs.getString("password"));
                }
            }

            if(user != null){
                nameField.setText(user.getName());
                surnameField.setText(user.getSurname());
                usrField.setText(user.getUsr());
                mailField.setText(user.getMail());
                nationalityFiled.setText(user.getNationality());
                ageField.setText(user.getAge());
                pwdField.setText(user.getPassword());
                phoneField.setText(user.getPhone());
                if(user.getSex().matches("Male"))
                    maleCheckBox.setSelected(true);
                else
                    fmaleCheckBox.setSelected(true);

                maleCheckBox.setOnAction(eh);
                fmaleCheckBox.setOnAction(eh);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    private void disableAllField(){

        nameField.setEditable(false);
        surnameField.setEditable(false);
        usrField.setEditable(false);
        phoneField.setEditable(false);
        ageField.setEditable(false);
        nationalityFiled.setEditable(false);
        mailField.setEditable(false);
        pwdField.setEditable(false);
        rpwdField.setEditable(false);
        fmaleCheckBox.setDisable(true);
        maleCheckBox.setDisable(true);
    }

    private void activeAllField(){

        nameField.setEditable(true);
        surnameField.setEditable(true);
        phoneField.setEditable(true);
        ageField.setEditable(true);
        nationalityFiled.setEditable(true);
        mailField.setEditable(true);
        pwdField.setEditable(true);
        rpwdField.setEditable(true);
        fmaleCheckBox.setDisable(false);
        maleCheckBox.setDisable(false);
    }

    private void updatePersonalInformations(){

        MySQLQuery query = MySQLQuery.getInstance();

        ArrayList<String> field = new ArrayList<String>(
                Arrays.asList(
                        "username",
                        "name",
                        "surname",
                        "phone",
                        "mail" ,
                        "sex",
                        "age" ,
                        "nationality",
                        "password"
                ));

        ArrayList<String> value = new ArrayList<String>(

                Arrays.asList(
                        user.getUsr(),
                        user.getName(),
                        user.getSurname(),
                        user.getPhone(),
                        user.getMail(),
                        user.getSex(),
                        user.getAge(),
                        user.getNationality(),
                        user.getPassword()
                ));

        if(user.getPassword().length() >= 8 && 18 <= Integer.parseInt(user.getAge()) && Integer.parseInt(user.getAge()) <= 99)
            query.update("users", field , value);

    }

    EventHandler eh = new EventHandler<ActionEvent>() {
        /**
         * Invoked when a specific event of the type for which this handler is
         * registered happens.
         *
         * @param event the event which occurred
         */
        @Override
        public void handle(ActionEvent event) {

            JFXCheckBox check = (JFXCheckBox) event.getSource();

            if(check.getId().matches("maleCheckBox") && maleCheckBox.isSelected())
                fmaleCheckBox.setSelected(false);

            else if(check.getId().matches("fmaleCheckBox") && fmaleCheckBox.isSelected())
                maleCheckBox.setSelected(false);
        }
    };



}

