package sample.view;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import sample.MainApp;
import sample.model.MySQLQuery;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

public class receivedMessagesWindowController {

    @FXML
    JFXListView receivedMessagesListView;

    @FXML
    JFXTextField authorTextField;

    @FXML
    JFXTextField dateAndTimeTextField;

    @FXML
    JFXButton openButton;


    private MainApp mainApp;

    private ArrayList<String> users = new ArrayList<String>();

    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
    }

    public void loadMessages() {

        MySQLQuery query = MySQLQuery.getInstance();
        ArrayList<String> lastMessage = new ArrayList<String>();

        ResultSet rs1 = query.select("messages", new ArrayList<String>(Arrays.asList("recipient")), new ArrayList<String>(Arrays.asList(mainApp.getUserID())));

        try {
            while (rs1.next()) {

                boolean exists = false;

                for (int i = 0; i < users.size(); i++) {

                    if (users.get(i).matches(rs1.getString("author"))) {
                        exists = true;
                        break;
                    }
                }

                if (!exists)
                    users.add(rs1.getString("author"));

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }


        for (int i = 0; i < users.size(); i++) {

            System.out.println(users.get(i));
            ResultSet dateAndTime = query.select("messages", new ArrayList<String>(Arrays.asList("author")), new ArrayList<String>(Arrays.asList(users.get(i))));

            try {

                dateAndTime.last(); //seleziona ultimo elemento
                lastMessage.add(dateAndTime.getString("dateAndTime"));
                dateAndTime.first();

            } catch (SQLException e) {
                e.printStackTrace();
            }



            try {

                while (dateAndTime.next()) {

                    FXMLLoader requestPaneLoader = new FXMLLoader();

                    if (i % 2 == 0)
                        requestPaneLoader.setLocation(mainApp.getClass().getResource("view/messagesWindow2.fxml"));
                    else
                        requestPaneLoader.setLocation(mainApp.getClass().getResource("view/messagesWindow.fxml"));

                    AnchorPane requestAnchorPane = (AnchorPane) requestPaneLoader.load();

                    int num = requestAnchorPane.getChildren().size();
                    boolean entered = false;


                    for (int l = 0; l < num; l++) {

                        if (requestAnchorPane.getChildren().get(l).getId() != null && requestAnchorPane.getChildren().get(l).getId().matches("authorTextField")) {
                            JFXTextField textField = (JFXTextField) requestAnchorPane.getChildren().get(l);
                            textField.setText(users.get(i));
                        }

                        if (requestAnchorPane.getChildren().get(l).getId() != null && requestAnchorPane.getChildren().get(l).getId().matches("openButton")) {
                            JFXButton jfxButton = (JFXButton) requestAnchorPane.getChildren().get(l);
                            jfxButton.setOnAction(openHandler);
                        }

                        if(requestAnchorPane.getChildren().get(l).getId() != null && requestAnchorPane.getChildren().get(l).getId().matches("dateAndTimeTextField")){
                            JFXTextField textField = (JFXTextField) requestAnchorPane.getChildren().get(l);
                            textField.setText(lastMessage.get(i));
                            entered = true;
                        }
                    }

                    if (entered) {
                        receivedMessagesListView.getItems().add(requestAnchorPane);
                        break;
                    }
                }


            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }


        }
    }

    public void showMessages(String author) {

        mainApp.initConversationWindow(author);
    }





    EventHandler openHandler = new EventHandler<ActionEvent>() {
        /**
         * Invoked when a specific event of the type for which this handler is
         * registered happens.
         *
         * @param event the event which occurred
         */
        @Override
        public void handle(ActionEvent event) {

            JFXButton button = (JFXButton) event.getSource();
            AnchorPane buttonPane = (AnchorPane) button.getParent();
            int num = buttonPane.getChildren().size();

            for(int i = 0; i < num; i++){

                if(buttonPane.getChildren().get(i).getId() != null && buttonPane.getChildren().get(i).getId().matches("authorTextField")) {
                    JFXTextField author = (JFXTextField) buttonPane.getChildren().get(i);
                    showMessages(author.getText());
                }
            }
        }
    };

}
