package sample.view;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import sample.MainApp;
import sample.model.MySQLQuery;
import sample.model.Users;
import sample.model.dao.DAOException;

import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;


public class registerController {


    //*-----------------Elementi che figurano nella scena realizzata con sceneBuilder-------------------*
    @FXML
    private JFXTextField nameField;

    @FXML
    private JFXTextField surnameField;

    @FXML
    private JFXTextField phoneField;

    @FXML
    private JFXTextField mailField;

    @FXML
    private JFXTextField usrField;

    @FXML
    private JFXPasswordField pwdField;

    @FXML
    private JFXPasswordField rpwdField;

    @FXML
    private  JFXTextField ageField;

    @FXML
    private  JFXTextField nationalityField;

    @FXML
    private JFXButton registerButton;

    @FXML
    private JFXCheckBox maleCheckBox;

    @FXML
    private JFXCheckBox fmaleCheckBox;

    //*-------------------------------------------------------------------------------------------------------*


    //  L'attributo in questione consente al controller di richiamare i metodi definiti nella classe "principale", ovvero la classe eseguibile
    private MainApp mainApp;

    //  Il metodo in questione associa il suddetto handler all'unica istanza della classe. Istanza che viene creata
    //  nell'istante in cui si esegue l'applicazione

    public void setMainApp(MainApp mainApp){

        this.mainApp = mainApp;
    }



    //  Con questa istruzione si stanno istanziando un numero di oggetti String pari al numero di parametri passati al metodo 'asList(...)'
    //  ogni oggetto ha come valore uno dei seguenti parametri ed è accessibile per mezzo di un handler. Quindi, l'ArrayList in questione avrà
    //  una dimensione pari al numero di elementi in parentesi.

    //  Questi domini sono fondamentali per verificare se l'indirizzo email fornito in fase di registrazione è autentico o meno.


    private ArrayList<String> mailDomainList = new ArrayList<String>(

            Arrays.asList(
                    "aol.com", "att.net", "comcast.net", "facebook.com", "gmail.com", "gmx.com", "googlemail.com",
                    "google.com", "hotmail.com", "hotmail.co.uk", "mac.com", "me.com", "mail.com", "msn.com",
                    "live.com", "sbcglobal.net", "verizon.net", "yahoo.com", "yahoo.co.uk", "email.com", "fastmail.fm",
                    "games.com" , "gmx.net", "hush.com", "hushmail.com", "icloud.com", "iname.com", "inbox.com", "lavabit.com",
                    "love.com" , "outlook.com", "pobox.com", "protonmail.ch", "protonmail.com", "tutanota.de", "tutanota.com", "tutamail.com", "tuta.io",
                    "keemail.me", "rocketmail.com" , "safe-mail.net", "wow.com" , "ygm.com", "ymail.com" , "zoho.com", "yandex.com",
                    "bellsouth.net", "charter.net", "cox.net", "earthlink.net", "juno.com", "btinternet.com", "virginmedia.com",
                    "blueyonder.co.uk", "freeserve.co.uk", "live.co.uk", "ntlworld.com", "o2.co.uk", "orange.net", "sky.com",
                    "talktalk.co.uk", "tiscali.co.uk", "virgin.net", "wanadoo.co.uk", "bt.com", "sina.com", "sina.cn", "qq.com", "naver.com",
                    "hanmail.net", "daum.net", "nate.com", "yahoo.co.jp", "yahoo.co.kr", "yahoo.co.id", "yahoo.co.in", "yahoo.com.sg",
                    "yahoo.com.ph", "163.com", "yeah.net", "126.com", "21cn.com", "aliyun.com", "foxmail.com",
                    "hotmail.fr", "live.fr", "laposte.net", "yahoo.fr", "wanadoo.fr", "orange.fr", "gmx.fr", "sfr.fr", "neuf.fr", "free.fr",
                    "gmx.de", "hotmail.de", "live.de", "online.de", "t-online.de", "web.de", "yahoo.de",
                    "libero.it", "virgilio.it", "hotmail.it", "aol.it", "tiscali.it", "alice.it", "live.it", "yahoo.it",
                    "email.it", "tin.it", "poste.it", "teletu.it", "mail.ru", "rambler.ru", "yandex.ru", "ya.ru", "list.ru",
                    "hotmail.be", "live.be", "skynet.be", "voo.be", "tvcablenet.be", "telenet.be",
                    "hotmail.com.ar", "live.com.ar", "yahoo.com.ar", "fibertel.com.ar", "speedy.com.ar", "arnet.com.ar",
                    "yahoo.com.mx", "live.com.mx", "hotmail.es", "hotmail.com.mx", "prodigy.net.mx",
                    "yahoo.ca", "hotmail.ca", "bell.net", "shaw.ca", "sympatico.ca", "rogers.com",
                    "yahoo.com.br", "hotmail.com.br", "outlook.com.br", "uol.com.br", "bol.com.br", "terra.com.br", "ig.com.br",
                    "itelefonica.com.br", "r7.com", "zipmail.com.br", "globo.com", "globomail.com", "oi.com.br"
            )

    );


    //  Il metodo in questione viene eseguito nell'istante in cui si clicca sul bottone 'Sign Up' presente nella finestra di registrazione
    //  Esso effettua un check dei dati forniti, successivamente
    //  se il check ha esito positivo evoca il metodo 'addUser'. Questo viene eseguito se e solo se:

    //      il testo introdotto nel campo password coincide con quello introdotto nel campo Conferma Password
    //      il testo introdotto nel campo mail contiene una @

    @FXML
    private void checkField() {

        //  Questa variabile diventa 'true' se nel database figurano utenti con stesso nick e/o stessa email

        boolean exists = false;

        //  Questa variabile diventa 'true' se il dominio dell'indirizzo email fornito in fase di registrazione figura tra quelli associati al suddetto ArrayList

        boolean emailCheck = false;

        //  Quelli che seguono sono degli handler del tipo String. Ognuno referenzia un'area di memoria. Ogni area contiene un parametro tra quelli forniti in fase di registrazione

        String usr = usrField.getText(); // Campo username
        String Pass = pwdField.getText(); //    Campo password
        String RepeatPass = rpwdField.getText(); // Campo Conferma Password
        String mail = mailField.getText(); //   Campo Email
        String age = ageField.getText(); // Campo Age
        String nationality = nationalityField.getText(); // Campo Nationality


        //  Con questa istruzione si definisce un handler MySQLQuery e si fa in modo che possa referenziare l'unica istanza di questa classe

        MySQLQuery query = MySQLQuery.getInstance();

        //  Con questa istruzione si esegue un SELECT sulla tabella users

        ResultSet rs = query.select("users");


        //  Il seguente ciclo for consente di scorrere il suddetto ArrayList

        for (int i = 0; i < mailDomainList.size(); i++) {

            //  Se l'indirizzo email fornito presenta:
            //      come dominio quello attribuito all'istanza String referenziata dall'i-esimo handler
            //      una @
            //  allora la variabile 'emailCheck' viene settata a true e si esce dal ciclo for

            if (mail.contains(mailDomainList.get(i)) && mail.contains("@")) {
                emailCheck = true;
                break;
            }
        }


        //  Se il check ha avuto esito positivo si esegue quanto segue:

        if (emailCheck) {

            //  Verifica se l'utente esiste o meno. Se esiste 'exists' diventa true e non lo si aggiunge
            try {

                //  Si analizzano i campi 'username' e 'mail' delle tuple restituite dal suddetto SELECT
                //  se lo username e/o la mail fornita corrispondono con quelli presenti in una tupla
                //  la variabile 'exists' viene settata a true e si esce dal ciclo while

                while (rs.next()) {

                    if (rs.getString("username").matches(usr) || rs.getString("mail").matches(mail)) {
                        exists = true;
                        break;
                    }
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


        //Se:
        //non esistono altri utenti con le stesse credenziali
        //il contenuto del campo 'password' coincide con quello del campo 'ripeti password'
        //il check effettuato sull'indirizzo email ha avuto esito positivo

        //si aggiorna lo stage, sostituendo la pagina di registrazione con quella di login e successivamente viene evocato il metodo per aggiungere l'utente al databse
        if (!exists && Pass.matches(RepeatPass) && Pass.length() >= 8 && emailCheck && (maleCheckBox.isSelected() || fmaleCheckBox.isSelected()) && !nationality.isEmpty() && !age.isEmpty() && 18 <= Integer.parseInt(age) && Integer.parseInt(age) <= 99) {
            mainApp.initLoginWindow();
            addUser();
        } else {
            mainApp.initFailedRegistrationRequestWindow();

                Timer t = new java.util.Timer();
                t.schedule(
                        new java.util.TimerTask() {
                            @Override
                            public void run() {
                                Platform.runLater(() -> {
                                            mainApp.initLoginWindow();
                                        });
                                t.cancel();
                            }
                        },
                        2500);
        }
    }

    //  Il metodo in questione viene eseguito nell'istante in cui si clicca sul pulsante 'Login'
    //  esso consente di tornare alla schermata di login

    @FXML
    private void showLoginWindow(){
        mainApp.initLoginWindow();
    }


    //  Il metodo in questione ha il compito di richiedere il lancio di un INSERT per aggiungere al database i dati forniti dall'utente
    //  in fase di registrazione

    private void addUser(){

        //  Viene istanziato un oggetto del tipo Users e i suoi attributi vengono inizializzati con i dati forniti in fase di registrazione

        Users users = new Users();
        users.setName(nameField.getText());
        users.setSurname(surnameField.getText());
        users.setMail(mailField.getText());
        users.setPassword(pwdField.getText());
        users.setPhone(phoneField.getText());
        users.setUsr(usrField.getText());
        users.setNationality(nationalityField.getText());
        users.setAge(ageField.getText());

        //  Se il checkbox selezionato è il 'maleCheckBox', l'attributo 'sex' viene inizializzato con 'Male'
        //  Se invece il checkbox selezionato è il 'fmaleCheckBox', l'attributo 'sex' viene inizializzato con 'Fmale'

        if(maleCheckBox.isSelected())
            users.setSex("Male");

        else if(fmaleCheckBox.isSelected())
            users.setSex("Fmale");

        try {

            //  Con questa istruzione si definisce un handler MySQLQuery e si fa in modo che possa referenziare l'unica istanza di questa classe

            MySQLQuery query = MySQLQuery.getInstance();

            //  Con questa istruzione si sfrutta l'istanza della classe MySQL per eseguire un INSERT

            query.insert(users , "users");

        } catch (DAOException e) {
            e.printStackTrace();
        }
    }


    //  Il metodo in questione viene eseguito in maniera automatica nell'istante in cui viene istanziato un oggetto di questa classe
    //  ciò accade quando viene eseguito il metodo initRegister

    public void initialize(){

        // Le seguenti istruzioni consentono di attribuire all'handler definito dopo il metodo in questione il ruolo
        // di gestore dell'evento 'onAction', ovvero quell'evento che si presenta ogni qual volta s'interagisce con
        //  il checkbox

        fmaleCheckBox.setOnAction(eh);
        maleCheckBox.setOnAction(eh);

    }

    //  Il seguente handler fa in modo che quando si seleziona il CheckBox 'F'/'M' , il CheckBox 'M'/'F' se selezionato,
    //  viene deselezionato.

    EventHandler eh = new EventHandler<ActionEvent>() {
        /**
         * Invoked when a specific event of the type for which this handler is
         * registered happens.
         *
         * @param event the event which occurred
         */
        @Override
        public void handle(ActionEvent event) {

            JFXCheckBox checkBox = (JFXCheckBox) event.getSource();

            if(checkBox.getId().matches("maleCheckBox") && fmaleCheckBox.isSelected())
                fmaleCheckBox.setSelected(false);

            else if(checkBox.getId().matches("fmaleCheckBox") && maleCheckBox.isSelected())
                maleCheckBox.setSelected(false);

        }

    };


}
