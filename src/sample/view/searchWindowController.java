package sample.view;

import com.jfoenix.controls.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import sample.MainApp;
import sample.model.MySQLQuery;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class searchWindowController {

    private MainApp mainApp;
    ObservableList<String> AirportBoxList;
    ObservableList<String> CountryBoxList;


    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
    }

    @FXML
    JFXComboBox countryBox;

    @FXML
    JFXComboBox airportBox;

    @FXML
    JFXTextField streetField;

    @FXML
    JFXTextField townField;

    @FXML
    JFXDatePicker dateField;

    @FXML
    JFXTimePicker timeField;

    @FXML
    JFXButton searchRequestButton;

    @FXML
    JFXCheckBox lookForCheckBox;

    @FXML
    JFXCheckBox offerCheckBox;

    @FXML
    JFXCheckBox shareCheckBox;

    @FXML
    JFXCheckBox distanceCheckBox;


    boolean test = true;

    public void initialize(){
        try {

            //Con questa istruzione si definisce un handler MySQLQuery e si fa in modo che possa referenziare l'unica istanza di questa classe
            MySQLQuery query = MySQLQuery.getInstance();

            //Con questa istruzione si esegue un SELECT sulla tabella airport
            ResultSet rs = query.select("airport");

            //Con la seguente istruzione si sta realizzando una lista di handler del tipo String
            //questi handler vengono utilizzati per referenziare le informazioni recuperate con il suddetto SELECT

            ArrayList<String> tempList = new ArrayList<String>();

            //Il seguente ciclo while consente di scorrere le tuple restituite dal suddetto SELECT,
            //Dei dati contenuti all'interno della tupla si recupera soltanto la nazione.
            //ad ogni iterazione aumenta il numero di handler presenti nell'Array. Ogni handler introdotto
            //viene utilizzato per referenziare la nazione che figura nella tupla in esame

            while(rs.next()){
                tempList.add(rs.getString("country"));
            }

            //Dato che ogni nazione figura in più tuple, si utilizza il metodo distinct().collect(Collectors.toList())
            //per creare un ArrayList privo di duplicati. In questo modo la stessa stringa non risulta essere allocata più volte

            ArrayList<String> countryList = (ArrayList<String>) tempList.stream().distinct().collect(Collectors.toList());

            //La classe FXCollections fornisce un metodo per creare un'ObservableList a partire da un'ArrayList. Il metodo
            //in questione è observableArrayList(countryList). Ultimata la creazione la lista viene associata ai due ComboBox
            //che figurano nella pagina per aggiungere richieste al database.

            CountryBoxList = FXCollections.observableArrayList(countryList);
            countryBox.setItems(CountryBoxList);

            offerCheckBox.setOnAction(offerLookShareHandler);
            lookForCheckBox.setOnAction(offerLookShareHandler);
            shareCheckBox.setOnAction(offerLookShareHandler);


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void sortAirportList(){


        if(countryBox.getSelectionModel().getSelectedItem() != null) {
            try {

                MySQLQuery query = MySQLQuery.getInstance();
                ResultSet rs;

                String country = (String) countryBox.getSelectionModel().getSelectedItem();
                rs = query.select("airport");

                ArrayList<String> list = new ArrayList<String>();

                while (rs.next()) {

                    if (rs.getString(2).matches(country))
                        list.add(rs.getString(1));
                }


                AirportBoxList = FXCollections.observableArrayList(list);
                airportBox.setItems(AirportBoxList);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void search(){

        MySQLQuery query = MySQLQuery.getInstance();
        Map address = Map.getInstance();

        ArrayList<String> field = new ArrayList<String>();
        ArrayList<String> value = new ArrayList<String>();

        field.add("toCountry");
        value.add((String) countryBox.getSelectionModel().getSelectedItem());
        field.add("toAirport");
        value.add((String) airportBox.getSelectionModel().getSelectedItem());
        field.add("address");
        value.add(address.adjustAddress(streetField.getText(), townField.getText(),"address"));
        field.add("town");
        value.add(address.adjustAddress(streetField.getText(), townField.getText(),"town"));
        field.add("arriveDate");
        value.add(dateField.getValue().toString());
        field.add("arriveTime");
        value.add(timeField.getValue().toString());


        if(lookForCheckBox.isSelected() || shareCheckBox.isSelected() || offerCheckBox.isSelected()) {

            field.add("serviceType");

            if (lookForCheckBox.isSelected() && shareCheckBox.isSelected()) {
                value.add("shareAndlook");
            } else if (lookForCheckBox.isSelected() && !shareCheckBox.isSelected()) {
                value.add("look");
            } else if (!lookForCheckBox.isSelected() && shareCheckBox.isSelected()) {
                value.add("share");
            } else if (offerCheckBox.isSelected()) {
                value.add("offer");
            }
        }



        ResultSet rs;
        if(!distanceCheckBox.isSelected())
            rs = query.select("requests" , field , value);

        else {

            rs = query.select("requests");

            try {

                while(rs.next()) {

                    Map prova = Map.getInstance();

                    if(!prova.verifyDistance(value.get(3) + " " + value.get(2), rs.getString("town") + " " + rs.getString("address")))
                        rs.deleteRow();

                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            }

        try {
            rs.beforeFirst();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        mainApp.initResultsSearchWindow(rs, value.get(2), value.get(3));


        }




    EventHandler offerLookShareHandler = new EventHandler<ActionEvent>() {
        /**
         * Invoked when a specific event of the type for which this handler is
         * registered happens.
         *
         * @param event the event which occurred
         */
        @Override
        public void handle(ActionEvent event) {

            JFXCheckBox eh = (JFXCheckBox) event.getSource();

            if (eh.getId().matches("offerCheckBox")) {

                if (lookForCheckBox.isSelected())
                    lookForCheckBox.setSelected(false);

                if (offerCheckBox.isSelected())
                    shareCheckBox.setSelected(false);
            } else if ((eh.getId().matches("lookForCheckBox") || eh.getId().matches("shareCheckBox")) && offerCheckBox.isSelected())
                offerCheckBox.setSelected(false);

        }
    };


}
